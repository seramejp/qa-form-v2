@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img src="{{url('/images/user-32.png')}}" alt=""> Users
		</li>

		<li class="active">index</li>
	    <li><a href="{{ url('/admin/user/create') }}">create</a></li>
	</ol>
@endsection

@section('below-main-content')
	<div class="row animate-box">
		<table class="table table-hover">
			<thead>
				<tr>
					<th></th>
					<th>Name</th>
					<th>Username</th>
					<th>Access Type</th>
					<th>Created At</th>
				</tr>
			</thead>
			<tbody>
				@foreach($users as $user)
					<tr>
						<td><a href="{{ url('/admin/user/' . $user->id . '/edit') }}"><i class="ti-pencil-alt"></i></a></td>
						<td>{{$user->name}}</td>
						<td>{{$user->username}}</td>
						<td>{{  ($user->sys_accesslevel == "1300135" ? "IT Admin" : ($user->sys_accesslevel == "1" ? "QA Admin" : ($user->sys_accesslevel == "2" ? "Agent User" : "Out of this world!") ) ) }}</td>
						<td>{{$user->created_at->format('d-m-Y h:i:s')}}</td>
					</tr>
				@endforeach
			</tbody>

			<tfoot>
				<th colspan="5" rowspan="1">
					<a href="{{ url('/admin/user/create') }}" class="btn btn btn-special"><i class="ti-plus"></i> New User</a>
				</th>
			</tfoot>
		</table>
    </div>

    
@endsection

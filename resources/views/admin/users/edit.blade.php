@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img src="{{url('/images/user-32.png')}}" alt=""> Users
		</li>

		<li><a href="{{ url('/admin/users') }}">index</a></li>
	    <li class="active">edit user</li>
	</ol>
@endsection

@section('below-main-content')
	<div class="row animate-box">
		<h2>Modifying User</h2>
		<form id="formEditUser" method="POST" action="{{url('/admin/user/' . $user->id . '/update')}}" class="text-left" enctype="multipart/form-data">
			{{ method_field('PATCH') }}
			{{ csrf_field() }}

			<div class="form-group">
				<label for="">Name</label>
				<input name="name" type="text" class="form-control" value="{{ $user->name }}">
			</div>

			<div class="form-group">
				<label for="">Username</label>
				<input name="username" type="text" class="form-control" value="{{ $user->username }}">
			</div>

			<div class="form-group">
				<label for="">Password</label>
				<input name="password" type="password" class="form-control" value="">
			</div>

			<div class="form-group">
				<label for="">Access Type</label>
				<select name="sys_accesslevel" class="form-control">
					<option value="1">QA Admin</option>
					<option value="2">Agent User</option>
				</select>
			</div>

			<div class="row">
                <div class="col-md-4 pull-right">
                    <button class="btn btn-primary btn-block" type="submit">Save</button>
                </div>
                <div class="col-md-4 pull-right">
                    <a class="btn btn-default btn-block" href="{{url('/admin/users')}}">Cancel</a>
                </div>
            </div>
		</form>

    </div>
@endsection

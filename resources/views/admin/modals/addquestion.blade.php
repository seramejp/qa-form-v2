<!-- Create a new Team -->
<div class="modal fade" id="mAddQuestion" tabindex="-1" role="dialog" aria-labelledby="mAddQuestion_Label" aria-hidden="true" style="position: fixed; top: 20%;">
    
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="fAddQuestion">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="mAddQuestion_Label">Create New Question</h4>
                    <h4 style="margin-bottom: 10px !important;"><small>add a question</small></h4>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">

                                <div class="form-group">
                                    <label for="" class="">Team/Department Name</label>
                                    <select id="aq-team" class="form-control selectpicker" data-size="8" data-live-search="true">
                                        @if(isset($teams))
                                            @foreach($teams as $team)
                                                <option value="{{$team->id}}">{{$team->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Category</label><br>
                                    <select id="aq-category" class="form-control selectpicker" data-size="8" data-live-search="true" data-hide-disabled="true">
                                        @if(isset($categories))
                                            @foreach($categories as $category)
                                                <option value="{{$category->id}}" data-teamid="{{$category->team_id}}">{{$category->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Question</label><br>
                                    <input id="aq-name" type="text" class="form-control" placeholder="ask me anything!">
                                </div>

                                <div class="form-group">
                                    <label for="">Points</label>
                                    <input id="aq-points" type="number" min="1" class="form-control" placeholder="Assign points to this question">
                                </div>

                                <div class="form-group">
                                    <label for="aq-guide">Please provide a guide to this question.</label>
                                    <textarea id="aq-guide" rows="3" class="form-control tb-white-bg" placeholder="What is this question all about?"></textarea>
                                </div>
                            
                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <input id="aq-save" class="btn btn-primary btn-outline" value="Save" data-dismiss="modal" onclick="createQuestion();">
                    <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </form>
    </div>
</div>
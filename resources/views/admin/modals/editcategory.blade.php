<!-- Create a new Team -->
<div class="modal fade" id="mEditCategory" tabindex="-1" role="dialog" aria-labelledby="mEditCategory_Label" aria-hidden="true" style="position: fixed; top: 20%;">
    
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="fEditCategory">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="mEditCategory_Label">Modify this Category</h4>
                    <h4 style="margin-bottom: 10px !important;"><small>modify the information contained in this category</small></h4>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                                
                                <input type="text" id="ec-categoryid" class="hidden" value="">
                                <input type="text" id="ec-rowindex" class="hidden" value="">

                                <div class="form-group">
                                    <label for="" class="">Team/Department Name</label>
                                    <select id="ec-team" class="form-control">
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Category Name</label><br>
                                    <input id="ec-name" type="text" class="form-control" placeholder="what should this category be named?">
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Description</label><br>
                                    <textarea id="ec-descr" placeholder="brief description of this category" class="form-control" cols="15" rows="3"></textarea>
                                </div>

                                <div id="textwrapper" class="">
                                    <div class="form-group">
                                        <label for="">Remarks</label>
                                        <textarea id="ec-remarks" placeholder="some notes here" class="form-control" cols="15" rows="3"></textarea>
                                    </div>
                                </div>
                            
                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <input id="ec-save" class="btn btn-primary btn-outline" value="Save" data-dismiss="modal" onclick="updateCategory();">
                    <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </form>
    </div>
</div>
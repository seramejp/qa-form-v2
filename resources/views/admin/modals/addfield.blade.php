<!-- Create a new Team -->
<div class="modal fade" id="mAddField" tabindex="-1" role="dialog" aria-labelledby="mAddField_Label" aria-hidden="true" style="position: fixed; top: 20%;">
    
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="fAddField">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="mAddField_Label">Create New Field</h4>
                    <h4 style="margin-bottom: 10px !important;"><small>fields for the general information tab</small></h4>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">

                                <input id="af-tablehost" type="text" class="hidden">
                                <input id="af-teamid" type="text" class="hidden">

                                <div class="form-group">
                                    <label for="" class="">Label</label>
                                    <input id="af-label" name="label" type="text" placeholder="what do you want to call this field?" class="form-control" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Type</label><br>
                                    <label class="radio-inline"><input type="radio" name="optradio" value="date" checked="">Date</label>
                                    <label class="radio-inline"><input type="radio" name="optradio" value="text">Text input</label>
                                    <label class="radio-inline"><input type="radio" name="optradio" value="select">Select option</label>
                                </div>

                                <div id="twrapper" class="hidden">
                                    <div class="form-group">
                                        <label for="">Placeholder</label>
                                        <input id="af-placeholder" name="placeholder" type="text" placeholder="this is a placeholder" class="form-control">
                                    </div>
                                </div>


                                <div id="selectwrapper" class="hidden">
                                    <div class="form-group">
                                        <label for="">Options <small><em>separate options with a semi-colon e.g. tanks; goons; guns</em></small> </label>
                                        <input id="af-options" name="options" type="text" placeholder="small; medium; large; xlarge;" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="">Order</label>
                                    <input id="af-order" type="number" class="form-control" min="1" value="4">
                                </div>

                                
                            
                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                <input id="af-save" class="btn btn-primary btn-outline" value="Save" data-dismiss="modal" onclick="createField();">
                    <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </form>
    </div>
</div>
<!-- Create a new Team -->
<div class="modal fade" id="mAddCategory" tabindex="-1" role="dialog" aria-labelledby="mAddCategory_Label" aria-hidden="true" style="position: fixed; top: 20%;">
    
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="fAddCategory">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="mAddCategory_Label">Create New Category</h4>
                    <h4 style="margin-bottom: 10px !important;"><small>add a category for the questions</small></h4>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">

                                <input id="ac-tablehost" type="text" class="hidden">

                                <div class="form-group">
                                    <label for="" class="">Team/Department Name</label>
                                    <select id="ac-team" class="form-control selectpicker" data-size="8" data-live-search="true">   
                                        @if(isset($teams))
                                            @foreach($teams as $team)
                                                <option value="{{$team->id}}">{{$team->name}}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Category Name</label><br>
                                    <input id="ac-name" type="text" class="form-control" placeholder="what should this category be named?">
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Description</label><br>
                                    <textarea id="ac-descr" placeholder="brief description of this category" class="form-control" cols="15" rows="3"></textarea>
                                </div>

                                <div id="textwrapper" class="">
                                    <div class="form-group">
                                        <label for="">Remarks</label>
                                        <textarea id="ac-remarks" placeholder="some notes here" class="form-control" cols="15" rows="3"></textarea>
                                    </div>
                                </div>
                            
                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <input id="ac-save" class="btn btn-primary btn-outline" value="Save" data-dismiss="modal" onclick="createCategory();">
                    <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </form>
    </div>
</div>
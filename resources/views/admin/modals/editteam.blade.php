<!-- Create a new Team -->
<div class="modal fade" id="mEditTeam" tabindex="-1" role="dialog" aria-labelledby="mEditTeam_Label" aria-hidden="true" style="position: fixed; top: 20%;">
    
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="fEditTeam">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="mEditTeam_Label">Edit this Team</h4>
                    <h4 style="margin-bottom: 10px !important;"><small>modify this department</small></h4>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                                
                                <input type="text" id="et-teamid" class="hidden" value="">
                                <input type="text" id="et-rowindex" class="hidden" value="">
                                <input type="text" id="et-tablehost" class="hidden" value="">

                                <div class="form-group">
                                    <label for="" class="">Team/Department Name</label>
                                    <input id="et-name" type="text" placeholder="name that team, e.g. IT, QA" class="form-control" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Description</label><br>
                                    <textarea id="et-descr" placeholder="brief description of this team/department" class="form-control" cols="15" rows="3"></textarea>
                                </div>

                                <div id="textwrapper" class="">
                                    <div class="form-group">
                                        <label for="">Remarks</label>
                                        <textarea id="et-remarks" placeholder="some notes here" class="form-control" cols="15" rows="3"></textarea>
                                    </div>
                                </div>
                            
                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <input id="at-save" class="btn btn-primary btn-outline" value="Save" data-dismiss="modal" onclick="updateTeam();">
                    <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </form>
    </div>
</div>
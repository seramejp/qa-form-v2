<!-- Create a new Team -->
<div class="modal fade" id="mAddTeam" tabindex="-1" role="dialog" aria-labelledby="mAddTeam_Label" aria-hidden="true" style="position: fixed; top: 20%;">
    
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="fAddTeam">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="mAddTeam_Label">Create New Team</h4>
                    <h4 style="margin-bottom: 10px !important;"><small>add a new department</small></h4>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                                
                                <input type="text" id="at-tablehost" class="hidden" value="">

                                <div class="form-group">
                                    <label for="" class="">Team/Department Name</label>
                                    <input id="at-name" type="text" placeholder="name that team, e.g. IT, QA" class="form-control" autofocus>
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Description</label><br>
                                    <textarea id="at-descr" placeholder="brief description of this team/department" class="form-control" cols="15" rows="3"></textarea>
                                </div>

                                <div id="textwrapper" class="">
                                    <div class="form-group">
                                        <label for="">Remarks</label>
                                        <textarea id="at-remarks" placeholder="some notes here" class="form-control" cols="15" rows="3"></textarea>
                                    </div>
                                </div>
                            
                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <input id="at-save" class="btn btn-primary btn-outline" value="Save" data-dismiss="modal" onclick="createTeam();">
                    <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </form>
    </div>
</div>
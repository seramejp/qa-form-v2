<!-- Create a new Team -->
<div class="modal fade" id="mEditQuestion" tabindex="-1" role="dialog" aria-labelledby="mEditQuestion_Label" aria-hidden="true" style="position: fixed; top: 20%;">
    
    <div class="modal-dialog">

        <!-- Modal content-->
        <form id="fEditQuestion">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="mEditQuestion_Label">Edit this question</h4>
                    <h4 style="margin-bottom: 10px !important;"><small>you gotta ask what you gotta ask</small></h4>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                                
                                <input id="eq-rowindex" type="text" class="hidden">
                                <input id="eq-questionid" type="text" class="hidden">

                                <div class="form-group">
                                    <label for="" class="">Team/Department Name</label>
                                    <select id="eq-team" class="form-control"></select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Category</label><br>
                                    <select id="eq-category" class="form-control">
                                        @if(isset($team))
                                            @if($team->categories->count() > 0)
                                                @foreach($team->categories as $category)
                                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                                @endforeach
                                            @endif
                                        @endif
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="" class="">Question</label><br>
                                    <input id="eq-name" type="text" class="form-control" placeholder="ask me anything!">
                                </div>

                                <div class="form-group">
                                    <label for="">Points</label>
                                    <input id="eq-points" type="number" min="1" class="form-control" placeholder="Assign points to this question">
                                </div>

                                <div class="form-group">
                                    <label for="eq-guide">Please provide a guide to this question.</label>
                                    <textarea id="eq-guide" rows="3" class="form-control tb-white-bg" placeholder="What is this question all about?"></textarea>
                                </div>
                            
                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <input id="eq-save" class="btn btn-primary btn-outline" value="Save" data-dismiss="modal" onclick="updateQuestion();">
                    <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </form>
    </div>
</div>
<div class="modal fade" id="modalConfirmDeleteAsk" tabindex="-1" role="dialog" aria-labelledby="modalConfirmDeleteLabel" aria-hidden="true" style="position: fixed; top: 20%;">
        
        <div class="modal-dialog">


            <div class="modal-content">
                <div class="modal-header" style="background-color: black;">
                    <button type="button" class="close" data-dismiss="modal" style="color: red !important;">&times;</button>
                    <h4 class="modal-title text-white"><span class="text-danger"><i class="ti-alert"></i></span> Do you really want to <span class="text-danger"><strong>delete</strong></span> this category?</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">
                        
                            
                                <div class="form-group">
                                    <label for="">Please make sure you are doing the right thing. Do you really mean to "delete" and not "modify"? This operation is <span class="text-danger">not reversible!</span>. <br>Deleting this category will also delete the questions under it.</span>
                                    </label>
                                </div>

                                
                            
                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <a id="btnYesDeleteCategory" href="" role="button" class="btn btn-danger btn-outline">Yes, I am sure!</a>
                    <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Cancel</button>
                </div>

            </div>
        </div>
</div>
@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img src="{{url('/images/question-32.png')}}" alt=""> Questions
		</li>

		<li class="active">index</li>
	    <li><a href="" data-toggle='modal' data-target="#mAddQuestion">create</a></li>
	    <li><a href="{{ url('/admin/questions/delete') }}" class="text-red">delete</a></li>
	</ol>
@endsection

@section('below-main-content')
	<div class="row animate-box">
            
			<table class="table table-hover" id="questionstable" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th width="4%"></th>
						<th>Team Name</th>
						<th>Category</th>
						<th>Question</th>
						<th>Points</th>
						<th>Guide</th>
					</tr>
				</thead>
				<tfoot>
					<th colspan="7" rowspan="1">
						<a href="" data-toggle='modal' data-target="#mAddQuestion" class="btn btn btn-special"><i class="ti-plus"></i> New Question</a>
					</th>
				</tfoot>
				
				<tbody>
				</tbody>
			</table>
    </div>
	
	@include ('admin.modals.addquestion')
	@include ('admin.modals.editquestion')
@endsection

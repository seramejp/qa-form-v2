@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img src="{{url('/images/question-32.png')}}" alt=""> Questions
		</li>

		<li><a href="{{ url('/admin/questions') }}">index</a></li>
	    <li><a href="" data-toggle='modal' data-target="#mAddQuestion">create</a></li>
	    <li class="active">delete</li>
	</ol>
@endsection

@section('below-main-content')
	<div class="row animate-box">
            <h3>Deleting a question will also delete all answers under it.</h3>
			<table class="table table-hover" id="questionstabledelete" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th width="4%"></th>
						<th>Team Name</th>
						<th>Category</th>
						<th>Question</th>
						<th>Points</th>
						<th>Guide</th>
					</tr>
				</thead>
				
				
				<tbody>
				</tbody>
			</table>
    </div>
	
	@include ('admin.modals.deletequestion')
@endsection

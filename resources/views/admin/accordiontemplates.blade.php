<template id="teamDetailsTemplate">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#accordionTeamDetails" href="#tdcollapse_"></a>
                <span class="pull-right">Total: <span id="points_" class="pts">0</span></span>
            </h4>
        </div>
        <div id="tdcollapse_" class="panel-collapse collapse">
            <div class="panel-body">
                <table id="tdtable_" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th><a id="std-mAddQuestion" href="" data-toggle="modal" data-target="#mAddQuestion" data-categoryid="" data-categoryname="" data-teamid="" data-teamname=""><i class="ti-plus"></i></a> Question</th>
                            <th>Points</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</template>

<template id="teamAccordion">
    <div class="panel-group" id="accordionTeamDetails">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordionTeamDetails" href="#tdcollapse_"></a>
                    <span class="pull-right">Total: <span id="points_" class="pts">0</span></span>
                    </h4>
                </div>
                <div id="tdcollapse_" class="panel-collapse collapse in">
                    <div class="panel-body">
                        <table id="tdtable_" class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th><a id="std-mAddQuestion" href="" data-toggle="modal" data-target="#mAddQuestion"  data-categoryid="" data-categoryname="" data-teamid="" data-teamname=""><i class="ti-plus"></i></a> Question</th>
                                    <th>Points</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
    </div>
</template>

<template id="fieldrows">
    <tr id="fieldrow_" onclick="editField();">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</template>
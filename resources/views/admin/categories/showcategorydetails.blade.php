@extends ('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img src="{{url('/images/categories-32.png')}}" alt=""> Categories
		</li>

		<li class=""><a href="{{ url('/admin/categories') }}">index</a></li>
	    <li class="active">{{$category->name}}</li>
	</ol>
@endsection

@section('below-main-content')
	<h3>Team: {{$category->team->name}}</h3><br>
	<h3>Category: {{$category->name}}</h3><br>
	<h3><a id="scd-mAddQuestion" href="" data-toggle="modal" data-target="#mAddQuestion"  data-categoryid="{{$category->id}}" data-categoryname="{{$category->name}}" data-teamid="{{$category->team_id}}" data-teamname="{{$category->team->name}}"><i class="ti-plus"></i></a> Questions
		<span class="pull-right">Total: 
			<span id="points_{{$category->id}}">{{$category->questions->sum('points')}}</span>
		</span>
	</h3>
	
	<div class="panel panel-default">
		<div class="panel-body">
			<table id="tdtable_{{$category->id}}" class="table table-striped table-hover">
				<thead>
					<tr>
						<th>Question</th>
                        <th>Points</th>
                        <th>Guide</th>
					</tr>
				</thead>
				<tbody>
					@if(count($category->questions) > 0)
						@foreach($category->questions as $question)
							<tr id="scdtrquestion_{{$question->id}}" onclick="javascript:editQuestion({{$question->id}});" style="cursor: pointer;" data-categoryid="{{$category->id}}" data-categoryname="{{$category->name}}" data-teamid="{{$category->team_id}}" data-teamname="{{$category->team->name}}">
								<td>{{$question->name}}</td>
								<td>{{$question->points}}</td>
								<td>{{$question->guide}}</td>
							</tr>
						@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>

	@include('admin.modals.addquestion')
	@include('admin.modals.editquestion')
@endsection

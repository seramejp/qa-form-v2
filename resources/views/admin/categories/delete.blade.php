@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img src="{{url('/images/categories-32.png')}}" alt=""> Categories
		</li>

		<li><a href="{{ url('/admin/categories') }}">index</a></li>
	    <li><a id="ac-mAddCategory" href="" data-toggle='modal' data-target="#mAddCategory">create</a></li>
	    <li class="active">delete</li>
	</ol>
@endsection

@section('below-main-content')
	<div class="row animate-box">
            <h3>Deleting a category will also delete all questions and answers under it.</h3>
			<table class="table table-hover" id="categoriestabledelete" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th width="4%"></th>
						<th>Team Name</th>
						<th>Category Name</th>
						<th>Description</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th></th>
						<th></th>
						<th>Team</th>
						<th>Category</th>
						<th>Description</th>
						<th>Remarks</th>
					</tr>
				</tfoot>
				
				<tbody>
				</tbody>
			</table>
    </div>

	@include('admin.modals.deletecategory')
@endsection

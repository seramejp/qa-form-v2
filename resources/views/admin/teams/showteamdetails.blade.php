@extends ('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img src="{{url('/images/teams-32.png')}}" alt=""> Teams
		</li>

		<li class=""><a href="{{ url('/admin/teams') }}">index</a></li>
	    <li class="active">{{$team->name}}</li>
	</ol>
@endsection

@section('below-main-content')
	<h3>Team: {{$team->name}}</h3><br>
	<h3><a id="std-mAddCategory" href="" data-toggle='modal' data-target="#mAddCategory" data-teamid="{{$team->id}}" data-teamname="{{$team->name}}"><i class="ti-plus"></i></a> Categories</h3>


		
		<div id="detailswrapper">
			@if(count($team->categories) > 0 )
				
				<div class="panel-group" id="accordionTeamDetails">
					
					@foreach($team->categories as $category)
						<div class="panel panel-default">
							<div class="panel-heading">
								<h4 class="panel-title">
									<a data-toggle="collapse" data-parent="#accordionTeamDetails" href="#tdcollapse_{{$category->id}}">
								{{ ucfirst($category->name) }}</a>
								<span class="pull-right">Total: <span id="points_{{$category->id}}">{{$category->questions->sum('points')}}</span></span>
								</h4>
							</div>
							<div id="tdcollapse_{{$category->id}}" class="panel-collapse collapse {{  $loop->first ? "in" : "" }}">
								<div class="panel-body">
									<table id="tdtable_{{$category->id}}" class="table table-striped table-hover">
										<thead>
											<tr>
												<th><a id="std-mAddQuestion" href="" data-toggle="modal" data-target="#mAddQuestion"  data-categoryid="{{$category->id}}" data-categoryname="{{$category->name}}" data-teamid="{{$team->id}}" data-teamname="{{$team->name}}"><i class="ti-plus"></i></a> Question</th>
												<th>Points</th>
												<th>Guide</th>
											</tr>
										</thead>
										<tbody>
											@foreach($category->questions as $question)
												<tr id="scdtrquestion_{{$question->id}}" onclick="javascript:editQuestion({{$question->id}});" style="cursor: pointer;" data-categoryid="{{$category->id}}" data-categoryname="{{$category->name}}" data-teamid="{{$category->team_id}}" data-teamname="{{$category->team->name}}">
													<td>{{$question->name}}</td>
													<td>{{$question->points}}</td>
													<td>{{$question->guide}}</td>
												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
						</div>
					@endforeach
		    	</div> <!-- End div accordion -->
		    @else
		    	<div id="nodatainfo">
		    		<h5>No data yet. Add data by clicking the <i class="ti-plus text-primary"></i> sign.</h5>
		    	</div>
		    @endif
	    </div>

	<div class="panel-group" id="accordionGeneralInfo">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a data-toggle="collapse" data-parent="#accordionGeneralInfo" href="#tdcollapse_general">General Information</a>
				</h4>
			</div>
			<div id="tdcollapse_general" class="panel-collapse collapse in">
				<div class="panel-body">
					<table id="tdtable_general" class="table table-striped table-hover">
						<thead>
							<tr>
								<th><a id="af-mAddField" href="" data-toggle="modal" data-target="#mAddField" data-teamid="{{$team->id}}"><i class="ti-plus"></i></a> Label</th>
								<th>Type</th>
								<th>Placeholder Text</th>
								<th>Options</th>
								<th>Order</th>
							</tr>
						</thead>
						<tbody>
							@foreach($fields as $field)
								<tr id="fieldrow_{{$field->id}}" onclick="editField({{$field->id}});">
									<td>{{$field->label}}</td>
									<td>{{$field->type}}</td>
									<td>{{$field->placeholder or ""}}</td>
									<td>{{$field->options or ""}}</td>
									<td>{{$field->order}}</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div> <!-- accordion general info -->
	
	@include ('admin.modals.addcategory')
	@include ('admin.modals.addquestion')
	@include ('admin.accordiontemplates')
	@include ('admin.modals.addfield')
	@include ('admin.modals.editquestion')
@endsection

@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img src="{{url('/images/teams-32.png')}}" alt=""> Teams
		</li>

		<li class="active">index</li>
	    <li><a id="at-mAddTeam" href="" data-toggle='modal' data-target="#mAddTeam">create</a></li>
	</ol>
@endsection

@section('below-main-content')
	<div class="row animate-box">
            
			<table class="table table-hover" id="teamstable" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th width="4%"></th>
						<th>Team Name</th>
						<th>Description</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<tfoot>
					<th colspan="5" rowspan="1">
						<a id="at-mAddTeam" href="" data-toggle='modal' data-target="#mAddTeam" class="btn btn btn-special"><i class="ti-plus"></i> New Team</a>
					</th>
				</tfoot>
				
				<tbody>
				</tbody>
			</table>
    </div>

    @include('admin.modals.addteam')
    @include('admin.modals.editteam')
@endsection

<div class="gtco-client">
    <div class="gtco-container">
        <div class="row">
            <div class="col-md-2 col-md-offset-1 text-center client col-sm-6 col-xs-6 col-xs-offset-0 col-sm-offset-0">
                <a href="{{url('/admin/teams')}}"><i class="ti-user"></i> Teams</a>
            </div>
            <div class="col-md-2 text-center client col-sm-6 col-xs-6 col-xs-offset-0 col-sm-offset-0">
                <a href="{{url('/admin/categories')}}"><i class="ti-notepad"></i> Categories</a>
            </div>
            <div class="col-md-2 text-center client col-sm-6 col-xs-6 col-xs-offset-0 col-sm-offset-0">
                <a href="{{url('/admin/questions')}}"><i class="ti-write"></i> Questions</a>
            </div>


            @if(Auth::user()->sys_accesslevel == "1300135")
                <div class="col-md-2 text-center client col-sm-6 col-xs-6 col-xs-offset-0 col-sm-offset-0">
                    <a href="{{url('/admin/users')}}"><i class="ti-id-badge"></i> Users</a>
                </div>
            @endif

            <div class="col-md-2 text-center client col-sm-6 col-xs-6 col-xs-offset-0 col-sm-offset-0">
                <a href="{{url('/admin/reports')}}"><i class="ti-clipboard"></i> Reports</a>
            </div>
        </div>
    </div>
</div>
@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img 
			src="{{url('/images/report-32.png')}}" alt=""> Reports
		</li>

		<li class="active">index</li>
	</ol>
@endsection

@section('below-main-content')

	<div class="row animate-box">
		<h2>List of Active Reports</h2>
		<div class="list-group">
			<div class="list-group-item">
				<h4 class="list-group-item-heading"><a href="{{url('/admin/reports/monthly')}}">QM Monthly Scores - All Staff</a>
					<span class="pull-right"><small><a href="{{url('/admin/reports/monthly')}}">View</a> <!-- | <a href="">Download</a> --></small></span>
				</h4>
				<p class="list-group-item-text" style="padding-left: 20px; padding-right: 20px;">QM Scores of all agents for the month of {{ date('F') }}. <br>Fields included are: Department, Staff Name, QA Auditor, Transaction Reference, Transaction Date, Audit Date, Week of Audit Date, Current Week QM Score</p>
			</div>

			<div class="list-group-item">
				<h4 class="list-group-item-heading"><a href="{{url('/admin/reports/agentscoreperweek')}}">Agent Score per Week</a>
					<span class="pull-right"><small><a href="{{url('/admin/reports/agentscoreperweek')}}">View</a> <!-- | <a href="">Download</a> --></small></span>
				</h4>
				<p class="list-group-item-text" style="padding-left: 20px; padding-right: 20px;">QA Scores of the agent per week, for the year {{ date('Y') }}. <br>Fields included are: Department, Staff Name, QA Auditor, Transaction Reference, Transaction Date, Audit Date, Week of Audit Date, Ave. Current week QM Score, Remarks</p>
			</div>

			<div class="list-group-item">
				<h4 class="list-group-item-heading"><a href="{{url('/admin/reports/qmweekly')}}">Quality Monitoring Report per Week</a>
					<span class="pull-right"><small><a href="{{url('/admin/reports/qmweekly')}}">View</a> <!-- | <a href="">Download</a> --></small></span>
				</h4>
				<p class="list-group-item-text" style="padding-left: 20px; padding-right: 20px;">This QA Report is as it appears in the <a href="https://docs.google.com/spreadsheets/d/1TjMeup9wJmH1sib5EmMt4wZtk8Zmv6XSMP1V3ka1oeU/edit#gid=339206843" target="_blank">Quality Monitoring Sheet 2017</a>. <br>Fields included are: Week, Audit By, Date Stamp, Transaction ID, Transaction Date, Person Involved, Score, Remarks</p>
			</div>

			<div class="list-group-item">
				<h4 class="list-group-item-heading"><a href="{{url('/admin/reports/personsofinterest')}}">Transactions with Errors</a>
					<span class="pull-right"><small><a href="{{url('/admin/reports/personsofinterest')}}">View</a> <!-- | <a href="">Download</a> --></small></span>
				</h4>
				<p class="list-group-item-text" style="padding-left: 20px; padding-right: 20px;">This QM Report will display only those persons with NO's in their questions, or those with Imperfect Scores. <br>Fields included are: Week, Audit By, Date Stamp, Transaction ID, Transaction Date, Person Involved, Score, Remarks</p>
			</div>


			<div class="list-group-item">
				<h4 class="list-group-item-heading"><a href="{{url('/admin/reports/deptweekly')}}">Department's Weekly Performance</a>
					<span class="pull-right"><small><a href="{{url('/admin/reports/deptweekly')}}">View</a> <!-- | <a href="">Download</a> --></small></span>
				</h4>
				<p class="list-group-item-text" style="padding-left: 20px; padding-right: 20px;">Average Score of all QM Samples per Department. <br>Fields included are: Department, Week Number and Average Scores</p>
			</div>


			<div class="list-group-item">
				<h4 class="list-group-item-heading"><a href="{{url('/admin/reports/agentweeklyperformance')}}">Agent's Weekly Performance per Department</a>
					<span class="pull-right"><small><a href="{{url('/admin/reports/agentweeklyperformance')}}">View</a> <!-- | <a href="">Download</a> --></small></span>
				</h4>
				<p class="list-group-item-text" style="padding-left: 20px; padding-right: 20px;">Average Score of all Agents per Department, per week. <br>Fields included are: Department, Agent name, Week Number and Average Scores</p>
			</div>


			<div class="list-group-item">
				<h4 class="list-group-item-heading"><a href="{{url('/admin/reports/checkpointsummary')}}">Checkpoint Summary</a>
					<span class="pull-right"><small><a href="{{url('/admin/reports/checkpointsummary')}}">View</a> <!-- | <a href="">Download</a> --></small></span>
				</h4>
				<p class="list-group-item-text" style="padding-left: 20px; padding-right: 20px;">You know when the audit for a particular question is NO? This is the summary. <br>Fields included are: Question, Agent name, Week Number and Count of No.</p>
			</div>


			<div class="list-group-item">
				<h4 class="list-group-item-heading"><a href="{{url('/admin/reports/deptmonthly')}}">Department's Monthly Performance</a>
					<span class="pull-right"><small><a href="{{url('/admin/reports/deptmonthly')}}">View</a> <!-- | <a href="">Download</a> --></small></span>
				</h4>
				<p class="list-group-item-text" style="padding-left: 20px; padding-right: 20px;">Average Score of all QM Samples per Department. <br>Fields included are: Department, Month and Average Scores</p>
			</div>

		</div>
		
    </div>

@endsection

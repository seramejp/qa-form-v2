<html> 
    <body> <h1>QM Sheet Weekly</h1>
         <br><h3>Person: {{$trans->person}}</h3>
         <br><h3>Transaction Date: {{$trans->transaction_date}}</h3>
         <br><h3>Total Score: {{$trans->sys_total}}</h3>

    @if(!empty($trans)) 

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Question</th>
                    <th>Answer</th>
                    <th>Score</th>
                </tr>
            </thead>
            <tbody>
                @if(!is_null($trans->team->questions))
                    
                    @foreach($trans->team->questions as $question)
                        <tr>
                            <td>{{ $question->name }}</td>
                            <td>{{ $trans->answers->where("question_id", $question->id)->first()->theanswer or "" }}</td>
                            <td>{{ $trans->answers->where("question_id", $question->id)->first()->score or "" }}</td>
                        </tr>
                    @endforeach
                    
                @endif
            </tbody>
        </table> 

    @endif 
    </body> 
</html>
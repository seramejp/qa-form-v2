@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img 
			src="{{url('/images/report-32.png')}}" alt=""> Reports
		</li>

		<li><a href="{{url('/admin/reports')}}">index</a></li>
		<li class="active">Monthly Scores</li>
	</ol>
@endsection

@section('below-main-content')

	<div class="row animate-box">
		<h2>QM Monthly Scores - {{ date('F') . ' ' .  date('Y') }}</h2>
		<table id="reporttablemonthly" class="table table-hover">
			<thead>
					<th>Department</th>
					<th>Staff Name</th>
					<th>QA Auditor</th>
					<th>Transaction Reference</th>
					<th>Transaction Date</th>
					<th>Audit Date</th>
					<th>Week of Audit Date</th>
					<th>Current week QM Score</th>
			</thead>

			<tbody>
				@foreach($reports as $trans)
					<tr>
						<td>{{ $trans->team->name }}</td>
						<td>{{ $trans->person }}</td>
						<td>{{ $trans->user->name }}</td>
						<td>{{ $trans->transaction_reference }}</td>
						<td>{{ $trans->transaction_date }}</td>
						<td>{{ $trans->created_at->format('d-m-Y') }}</td>
						<td>{{ $trans->created_at->weekOfYear }}</td>
						<td>{{ (null !== ($currWeekTransactionAve->where("person", $trans->person)->avg('sys_total')) ? ($currWeekTransactionAve->where("person", $trans->person)->avg('sys_total')) : 0 ) }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		
    </div>

@endsection

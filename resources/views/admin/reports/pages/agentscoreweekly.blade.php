@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img 
			src="{{url('/images/report-32.png')}}" alt=""> Reports
		</li>

		<li><a href="{{url('/admin/reports')}}">index</a></li>
		<li class="active">Agent Score per Week</li>
	</ol>
@endsection

@section('below-main-content')

	<div class="row animate-box">
		<h2>Agent Score per Week - {{ date('Y') }}</h2>
		<table id="reporttableagentweekly" class="table table-hover">
			<thead>
					<th>Department</th>
					<th>Staff Name</th>
					<th>QA Auditor</th>	
					<th>Transaction Reference</th>	
					<th>Transaction Date</th>
					<th>Audit Date</th>	
					<th>Week of Audit Date</th>
					<th>Current week QM Score</th>
					<th>Remarks</th>

			</thead>

			<tbody>
				@foreach($reports as $trans)
					<tr>
						<td>{{ $trans->id }}</td>
						<td>{{ $trans->person }}</td>
						<td>{{ $trans->user->name }}</td>
						<td>{{ $trans->transaction_reference }}</td>
						<td>{{ $trans->transaction_date }}</td>
						<td>{{ $trans->created_at->format('d-m-Y') }}</td>
						<td>{{ $trans->created_at->weekOfYear }}</td>
						<td>{{  (!is_null($transact->where("id", $trans->id)->first()["value"]) ? number_format($transact->where("id", $trans->id)->first()["value"], 2) : "--") }}</td>
						<td>{{ $trans->remarks }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		
    </div>

@endsection

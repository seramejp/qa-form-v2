@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img 
			src="{{url('/images/report-32.png')}}" alt=""> Reports
		</li>

		<li><a href="{{url('/admin/reports')}}">index</a></li>
		<li class="active">Department Weekly Performance</li>
	</ol>
@endsection

@section('below-main-content')

	<div class="row animate-box">
		<h2>Department's Weekly Performance Report - {{ date('Y') }}</h2>
		<h4>Current Week: <code>{{ Carbon\Carbon::now()->weekOfYear }}</code></h4>
		<table id="reporttableDeptWeekly" class="table table-hover">
			<thead>
					<th>Week #</th>
					@if(!is_null($teams))
						@foreach($teams as $team)
							<th>{{ strtoupper($team->name) }}</th>
						@endforeach
					@endif
			</thead>

			<tbody>
				@if(!is_null($transactions))
					@foreach($transactions as $key => $value)
						<tr>
							<td>{{ $key }}</td>
							@foreach($teams as $team)
								@if(!in_array($team->id, $value->pluck("team_id")->all()))
									<td> - </td>
								@else
									@foreach($value as $trans)
										@if($team->id == $trans->team_id)
											<td class="sum"><a class="weeklyreportdetails" href="" data-toggle="modal" data-target="#mViewDetails" data-teamname="{{strtoupper($team->name)}}" data-teamid="{{$team->id}}" data-weeknumber="{{$key}}" data-averagescore="{{number_format($trans->sys_total,0)}}">{{ number_format($trans->sys_total,0) }}</a></td>
										@endif
									@endforeach
								@endif
								
							@endforeach
						</tr>
					@endforeach
				@endif
			</tbody>
			<tfoot>
				<th>Average</th>
				@foreach($teams as $team)
					<th></th>
				@endforeach
			</tfoot>
		</table>
		
    </div>

	@include('admin.reports.pages.modals.viewdetails')
@endsection

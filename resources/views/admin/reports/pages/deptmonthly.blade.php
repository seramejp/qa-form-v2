@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img 
			src="{{url('/images/report-32.png')}}" alt=""> Reports
		</li>

		<li><a href="{{url('/admin/reports')}}">index</a></li>
		<li class="active">Department Monthly Performance</li>
	</ol>
@endsection

@section('below-main-content')

	<div class="row animate-box">
		<h2>Department's Monthly Performance Report - {{ date('Y') }}</h2>
		<h4>Current Month: <code>{{  date("F", strtotime(date("Y")."-".Carbon\Carbon::now()->month."-01")) }}</code></h4>
		<table id="reporttableDeptMonthly" class="table table-hover">
			<thead>
					<th>Month</th>
					@if(!is_null($teams))
						@foreach($teams as $team)
							<th>{{ strtoupper($team->name) }}</th>
						@endforeach
					@endif
			</thead>

			<tbody>
				@if(!is_null($transactions))
					@foreach($transactions as $key => $value)
						<tr>
							<td>{{  date("F", strtotime(date("Y")."-".$key."-01")) }}</td>
							@foreach($teams as $team)
								@if(!in_array($team->id, $value->pluck("team_id")->all()))
									<td> - </td>
								@else
									@foreach($value as $trans)
										@if($team->id == $trans->team_id)
											<td>{{ number_format($trans->sys_total,0) }}</td>
										@endif
									@endforeach
								@endif
								
							@endforeach
						</tr>
					@endforeach
				@endif
			</tbody>
			<tfoot>
				<th>Average</th>
				@foreach($teams as $team)
					<th></th>
				@endforeach
			</tfoot>
		</table>
		
    </div>

	@include('admin.reports.pages.modals.viewdetails')
@endsection

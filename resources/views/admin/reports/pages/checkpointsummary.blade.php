@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img 
			src="{{url('/images/report-32.png')}}" alt=""> Reports
		</li>

		<li><a href="{{url('/admin/reports')}}">index</a></li>
		<li class="active">Checkpoint Summary</li>
	</ol>
@endsection

@section('below-main-content')

	<div class="row animate-box">
		<h2>Checkpoint Summary</h2>

		<div class="searchWeek row">
			<form id="form_checkpointsummary" method="POST" action="{{url('/')}}" enctype="multipart/form-data">
        		{{ csrf_field() }}

				<div class="col-lg-5">
					<div class="form-group">
						<label for="">Week From</label>
						<input type="week" class="form-control" name="weekFrom" value="{{ Carbon\Carbon::now()->year . "-W". str_pad($weeknumbers[0], 2, '0', STR_PAD_LEFT)}}">
					</div>
				</div>
				<div class="col-lg-5">
					<div class="form-group">
						<label for="">Week To</label>
						<input type="week" class="form-control" name="weekTo" value="{{ Carbon\Carbon::now()->year . "-W". str_pad(end($weeknumbers), 2, '0', STR_PAD_LEFT)}}">
					</div>
				</div>
				<div class="col-lg-1">
					<label for=""></label>
					<button class="btn btn-default" type="button" onclick="form_checkpointsummary();">Go</button>
				</div>
			</form>
		</div>

		<table id="reportstableCheckpointSummary" class="table table-hover table-striped">
			<thead>
					<th>Question</th>
					<th>Agent Name</th>
					<th>Department</th>
					@foreach($weeknumbers as $wk)
						<th>{{ "Week " . $wk }}</th>
					@endforeach
			</thead>

			<tbody>
				@if(!is_null($transactions))
					@foreach($transactions as $key => $questions)
						@php
							$theQuestion = "";

							foreach ($questions as $item) {
								echo "<tr>";

								//$theQuestion == $key ? print "<td></td>" :print "<td>$key</td>";
								echo "<td>".$key."</td>";
								echo "<td>".$item->person."</td>";
								echo "<td>".$item->dept."</td>";

								foreach ($weeknumbers as $wk) {
									$cnt = $questions->where("weeknum", $wk)->where("person",$item->person)->count();

									echo "<td>". ($cnt>0?$cnt:"-") ."</td>";
								}

								$theQuestion = $key;
								echo "</tr>";
							}
						@endphp
						
					@endforeach
				@endif
				
			</tbody>
			<tfoot>
				<th></th>
				<th></th>
				<th></th>
				@foreach($weeknumbers as $wk)
					<th></th>
				@endforeach
			</tfoot>
			
		</table>
		
    </div>

@endsection

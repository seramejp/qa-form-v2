@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img 
			src="{{url('/images/report-32.png')}}" alt=""> Reports
		</li>

		<li><a href="{{url('/admin/reports')}}">index</a></li>
		<li class="active">Transactions with Errors</li>
	</ol>
@endsection

@section('below-main-content')

	<div class="row animate-box">
		<h2>Transactions with Errors - {{ date('Y') }}</h2>
		<table id="reporttablepoi" class="table table-hover">
			<thead>
					<th></th>
					<th></th>
					<th>Week</th>
					<th>Audit By</th>
					<th>Date Stamp</th>
					<th>Transaction ID</th>
					<th>Transaction Date</th>
					<th>Person Involved</th>
					<th>Score</th>
					<th>Remarks</th>
			</thead>

			<tbody>
				@foreach($poi as $trans)
					<tr>
						<td><a href="{{url('/admin/reports/qmweekly/export/'.$trans->id)}}"><i class="ti-export"></i></a></td>
						<td><a href="{{url('/staff/'. $trans->id .'/details')}}" target="_blank"><i class="ti-zoom-in"></i></a></td>
						<td>{{ $trans->created_at->weekOfYear }}</td>
						<td>{{ $trans->user->name }}</td>
						<td>{{ $trans->created_at->format('d-m-Y') }}</td>
						<td>{{ $trans->transaction_reference }}</td>
						<td>{{ $trans->transaction_date }}</td>
						<td>{{ $trans->person }}</td>
						<td>{{ $trans->sys_total }}</td>
						<td>{{ $trans->remarks }}</td>
					</tr>
				@endforeach
			</tbody>
		</table>
		
    </div>

@endsection

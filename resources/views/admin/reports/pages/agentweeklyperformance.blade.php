@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img 
			src="{{url('/images/report-32.png')}}" alt=""> Reports
		</li>

		<li><a href="{{url('/admin/reports')}}">index</a></li>
		<li class="active">Agent's Weekly Performance</li>
	</ol>
@endsection

@section('below-main-content')

	<div class="row animate-box">
		<h2>Agent's Weekly Performance</h2>

		<div class="searchWeek row">
			<form id="form_agentweeklyperformance" method="POST" action="{{url('/')}}" enctype="multipart/form-data">
        		{{ csrf_field() }}

				<div class="col-lg-5">
					<div class="form-group">
						<label for="">Week From</label>
						<input type="week" class="form-control" name="weekFrom" value="{{ Carbon\Carbon::now()->year . "-W". str_pad($weeknumbers[0], 2, '0', STR_PAD_LEFT)}}">
					</div>
				</div>
				<div class="col-lg-5">
					<div class="form-group">
						<label for="">Week To</label>
						<input type="week" class="form-control" name="weekTo" value="{{ Carbon\Carbon::now()->year . "-W". str_pad(end($weeknumbers), 2, '0', STR_PAD_LEFT)}}">
					</div>
				</div>
				<div class="col-lg-1">
					<label for=""></label>
					<button class="btn btn-default" type="button" onclick="form_agentweeklyperformance();">Go</button>
				</div>
			</form>
		</div>

		<table id="reportstableAgentWeeklyDetails" class="table table-hover">
			<thead>
					<th>Department</th>
					<th>Agent Name</th>
					@foreach($weeknumbers as $wk)
							<th>{{ "Week " . $wk }}</th>
					@endforeach
					<th><b>Ave</b></th>
					
			</thead>

			<tbody>
				@if(!is_null($transactions))
					@foreach($transactions as $key => $value)
						<!-- Per Team for each -->
						@php 
							$teamId = 0;
							$sumTotalRow = 0; 
						@endphp
						@foreach($value as $item)
							<tr>
									@php
										if ($teamId != $item->team_id) {
											echo "<td>". $item->team->name ."</td>";
											$teamId = $item->team_id;
										}
									@endphp

								<td>{{$key}}</td>
								@foreach($weeknumbers as $wk)
									@php
										$sys_total_final = 0;

										if (!empty($value->where("weeknum", $wk)->first()->sys_total)) {
												$sys_total_final = number_format($value->where("weeknum", $wk)->first()->sys_total, 2);
												$sumTotalRow += $sys_total_final;
										}
									@endphp
									<td>{{ $sys_total_final or 0 }}</td>
								@endforeach
									<td>{{ number_format($sumTotalRow/count($weeknumbers),2) }}</td>

							</tr>
						@endforeach
					@endforeach
				@endif
			</tbody>

			<tfoot>
				<th>Average</th>
				<th></th>
				@foreach($weeknumbers as $wk)
						<th></th>
				@endforeach
				<th></th>
			</tfoot>
		</table>
		
    </div>

	@include('admin.reports.pages.modals.viewdetails')
@endsection

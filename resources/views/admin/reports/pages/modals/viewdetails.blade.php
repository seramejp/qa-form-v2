<!-- Create a new Team -->
<div class="modal fade" id="mViewDetails" tabindex="-1" role="dialog" aria-labelledby="mViewDetails_Label" aria-hidden="true" style="position: fixed; top: 20%;">
    
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        
            <div class="modal-content">
                <div class="modal-header bg-transition">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="mViewDetails_Label">Viewing Details for <b><span id="title_teamname"></span></b></h4>
                    <h4 style="margin-bottom: 10px !important;"><small><span id="title_description"></span></small></h4>
                    
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10 col-lg-offset-1">

                               <table class="table table-hover" id="tableViewWeeklyPerformanceDetails">
                                   <thead>
                                       <th></th>
                                       <th>Who?</th>
                                       <th>Auditor</th>
                                       <th>Trans Ref</th>
                                       <th>Trans Date</th>
                                       <th>Audit Date</th>
                                       <th>Score</th>
                                   </thead>
                                   <tbody id="mViewDetails_table">
                                       
                                   </tbody>
                               </table> 

                        </div>
                       
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Close</button>
                </div>

            </div>
    </div>
</div>
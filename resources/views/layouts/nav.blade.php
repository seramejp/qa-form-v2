<nav class="gtco-nav" role="navigation">
    <div class="gtco-container">
        
        <div class="row">
            <div class="col-sm-2 col-xs-12">
                <div id="gtco-logo"><a href="{{url('/home')}}"><img src="{{url('images/logo.png')}}"> QA Form</a></div>
            </div>
            <div class="col-xs-10 text-right menu-1">
                <ul>
                    @if(Auth::user()->sys_accesslevel != "1" and  Auth::user()->sys_accesslevel != "1300135")
                        <li class="active has-dropdown">
                            <a href="#">Teams</a>
                            <ul class="dropdown">
                                @include('staff.teamlist')
                            </ul>
                        </li>
                    @endif
                    
                    <li>
                        <a href="{{ url('/logout') }}"
                            onclick="event.preventDefault();
                                     document.getElementById('logout-form').submit();">
                            @if(Auth::user()->sys_accesslevel == "1" or Auth::user()->sys_accesslevel == "1300135")
                                Admin Logout
                            @else
                                Logout
                            @endif
                        </a>

                        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </div>
        </div>
        
    </div>
</nav>
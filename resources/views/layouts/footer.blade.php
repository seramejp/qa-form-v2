<footer id="gtco-footer" class="gtco-section" role="contentinfo">
    <div class="gtco-container">
        <div class="row row-pb-md">
            <div class="col-md-4 gtco-widget gtco-footer-paragraph">
                <h3>QM Form</h3>
                <p>This is a brief description of what QM Form is, who the target roles are, and who does the ratings. This will be brief so you can read it without blinking twice.</p>
            </div>
            <div class="col-md-8 gtco-footer-link">
                <div class="row">
                    <div class="col-md-6">
                        <p>
                            <i class="ti-user"></i>&emsp; Mills IT Department <br>
                            <i class="ti-email"></i>&emsp;itsupport@millstrading.com.au <br>
                            <i class="ti-mobile"></i>&emsp; +63 925 823 5523 <br>
                            <i class="ti-mobile"></i>&emsp; +63 925 823 5525 <br>
                        </p>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
    <div class="gtco-copyright">
        <div class="gtco-container">
            <div class="row">
                <div class="col-md-6 text-left">
                    <p><small>&copy; 2017 Mills International Trading </small></p>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
</div>
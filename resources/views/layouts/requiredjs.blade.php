<script src="{{url('js/jquery.min.js')}}"></script>
<!-- jQuery Easing -->
<script src="{{url('js/jquery.easing.1.3.js')}}"></script>

<script src="{{url('js/datatables.min.js')}}"></script>
<script src="{{url('js/datatables-buttons.min.js')}}"></script>
<script src="{{url('js/jszip.min.js')}}"></script>
<script src="{{url('js/buttons.html5.min.js')}}"></script>



<!-- Bootstrap -->
<script src="{{url('js/bootstrap.min.js')}}"></script>

<!-- Select Picker -->
<script src="{{url('js/bootstrap-select.min.js')}}"></script>

<!-- Waypoints -->
<script src="{{url('js/jquery.waypoints.min.js')}}"></script>

<!-- Carousel -->
<script src="{{url('js/owl.carousel.min.js')}}"></script>

<!-- Magnific Popup -->
<script src="{{url('js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{url('js/magnific-popup-options.js')}}"></script>

<!-- Segment JS Plugin -->
<script src="{{url('js/segment.js')}}"></script>

<!-- Main -->
<script src="{{url('js/main.js')}}"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.9.1/sweetalert2.min.css">
<script src="//cdn.datatables.net/plug-ins/1.10.16/api/average().js"></script> 

<!-- Include a polyfill for ES6 Promises (optional) for IE11 and Android browser -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>

<!-- USER - DEFINED JS -->
<script>
	var globalUrl = "{{url('/')}}";
	var globalToken = "{{ csrf_token() }}";
	var globalCurrentTeam = "{{ (isset($team) ? $team->id : 0) }}";
</script>


<script src="{{url('js/custom.js')}}"></script>
<script src="{{url('js/customTeam.js')}}"></script>
<script src="{{url('js/customCategory.js')}}"></script>
<script src="{{url('js/customQuestion.js')}}"></script>
<script src="{{url('js/customField.js')}}"></script>
<script src="{{url('js/customHistory.js')}}"></script>
<script src="{{url('js/customReports.js')}}"></script>
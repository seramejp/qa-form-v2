@extends('layouts.app')

@section('main-content')
    <h3 class="text-white">{{$team->name}}</h3>
    <h5 class="text-white">The Guide - wherever you will go. </h5>
    <hr>
@endsection

@section('below-main-content')
    <form action="{{url('/staff/'.$team->id.'/save')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}

                    @if(count($team->categories) > 0)
                        @foreach($team->categories as $cat)
                            <div id="main_{{$cat->id}}" class="row">

                                <div class="panel list-group" style="background: transparent !important;">

                                        <h3 style="padding-top: 20px;"><i class="ti-light-bulb"></i> {{ ucfirst($cat->name) }}</h3>
                                        
                                        
                                            @foreach($cat->questions as $ask)

                                                <a class="list-group-item panel-list-group-item guide-hover" data-toggle="collapse" data-target="#q_{{$ask->id}}" data-parent="#main_{{$cat->id}}"> {{ ucfirst($ask->name) }}</a>
                                               
                                                <div id="q_{{$ask->id}}" class="sublinks collapse">
                                                    <a class="list-group-item small text-black" style="padding-left: 50px; background: transparent;"><strong>Points: <span class="text-danger">{{$ask->points}}</span></strong>. <br>
                                                        {{$ask->guide}}
                                                    </a>

                                                </div>
                                                    
                                            @endforeach
                                        
                                </div>
                            </div>

                        @endforeach

                        

                    @endif
            
    </form>
@endsection




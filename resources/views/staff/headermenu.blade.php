@if(isset($team))
	<div class="gtco-client">
	    <div class="gtco-container">
	        <div class="row">
	            <div class="col-md-2 col-md-offset-1 text-center client col-sm-6 col-xs-6 col-xs-offset-0 col-sm-offset-0">
			        <a href="{{url('/staff/'.$team->id.'/index')}}"><i class="ti-view-list-alt"></i> Index</a>
			    </div>
			    <div class="col-md-2 text-center client col-sm-6 col-xs-6 col-xs-offset-0 col-sm-offset-0">
			        <a href="{{url('/staff/'.$team->id.'/form')}}"><i class="ti-write"></i> Form</a>
			    </div>
			    <div class="col-md-2 text-center client col-sm-6 col-xs-6 col-xs-offset-0 col-sm-offset-0">
			        <a href="{{url('/staff/'.$team->id.'/history')}}"><i class="ti-book"></i> History</a>
			    </div>
			    <div class="col-md-2 text-center client col-sm-6 col-xs-6 col-xs-offset-0 col-sm-offset-0">
			        <a href="{{url('/staff/'.$team->id.'/guide')}}"><i class="ti-direction-alt"></i> Guide</a>
			    </div>
	        </div>
	    </div>
	</div>
@endif
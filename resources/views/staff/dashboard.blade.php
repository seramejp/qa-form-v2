@extends('layouts.app')



@section('main-content')
	<h1 class="text-center">Dashboard</h1>
@endsection


@section('below-main-content')
	<div class="row animate-box">
			<h4> Teams</h4>
            <div class="col-md-11 col-md-offset-1">
            	
				<table id="dbstaff-teams" class="table table-default table-striped table-hover">
					<thead>
							<th>ID</th>
							<th width="4%"></th>
							<th>Team</th>
							<th>Transactions</th>
					</thead>
					
					<tbody>
					</tbody>
					
				</table>
			</div>
    </div>

    <div class="row animate-box">
    		<h4> Top 10 Agents for the month of {{ date('F') }}</h4>
            <div class="col-md-11 col-md-offset-1">
            	
				<table class="table table-default table-striped table-hover">
					<thead>
							<th>#</th>
							<th></th>
							<th>Name</th>
							<th>Team</th>
							<th>Score</th>
					</thead>
					<tbody>
						@if($top10trans->count() > 0)
							@foreach($top10trans as $top10)
								<tr>
									<td>{{$loop->iteration}}</td>
									<td>
										<a href="{{url("/staff/".$top10->id."/details")}}" data-toggle="tooltip" data-placement="top" title="Click to view this forms details."><i class="ti-share"></i></a>
									</td>
									<td>{{$top10->person or "Anon"}}</td>
									<td>{{$top10->team->name}}</td>
									<td>{{$top10->sys_total}}</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="5">No data in table.</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
    </div>

    <div class="row animate-box">
    		<h4> Recently Added</h4>
            <div class="col-md-11 col-md-offset-1">
            	
				<table class="table table-default table-striped table-hover">
					<thead>
							<th></th>
							<th>Auditor</th>
							<th>Created At</th>
							<th>Team</th>
							<th>Person/Agent</th>
							<th>Score</th>
					</thead>
					<tbody>
						@if($mostRecent->count() > 0)
							@foreach($mostRecent as $recent)
								<tr>
									<td>
										<a href="{{url("/staff/".$recent->id."/details")}}" data-toggle="tooltip" data-placement="top" title="Click to view this forms details."><i class="ti-share"></i></a>
									</td>
									<td>{{ $recent->user->name }}</td>
									<td>{{ $recent->created_at->format('jS \\of F Y, h:i:s A') }}</td>
									<td>{{ $recent->team->name }}</td>
									<td>{{ $recent->person  or "Something"}}</td>
									<td>{{ $recent->sys_total}}</td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="6">No data in table.</td>
							</tr>
						@endif
					</tbody>
				</table>
			</div>
    </div>
	
	@include('admin.modals.addteam')
	@include('admin.modals.editteam')
@endsection
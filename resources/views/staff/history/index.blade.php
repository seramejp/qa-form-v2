@extends('layouts.app')

@section('main-content')
	<h3 class="text-white">{{$team->name}} History</h3>
	<h5 class="text-white">Their past, your history. This list contains everyone. Judger!</h5>
@endsection

@section('below-main-content')
	<div class="row animate-box">
		<div class="container">
		    <div class="row">    
		        <div class="col-xs-8 col-xs-offset-2">
				    <div class="input-group">
		                <div class="input-group-btn">
		                    <select id="searchConceptList" class="selectpicker">
								<option value="person">Person</option>
								<option value="transactionreference">Transaction Reference</option>
								<option value="dateencoded">Encoded After (date)</option>
							</select>
		                </div>
		                <input id="searchText" type="text" class="form-control" name="x" placeholder="Search term..." style="height:60px;">
		                <span class="input-group-btn">
		                    <button class="btn btn-default" type="button" onclick="loadHistoryTable();"><i class="ti-search"></i></button>
		                </span>
		            </div>
		        </div>
			</div>
		</div>
        
        <div id="spinner" class="col-md-offset-6 hidden">
            <i class="ti-reload"></i><br><br>
        </div>

        <table class="table table-hover" cellspacing="0" width="100%" id="formshistory">
            <caption><div id="buttoncontainer"></div></caption>
            <thead>
                <tr>
                    <th width="4%"></th>
                    <th>Encoded on</th>
                    <th>Who?</th>
                    <th>Trans Ref</th>
                    <th>Trans Date</th>
                    <th>Total Score</th>
                    <th>Remarks</th>
                </tr>
            </thead>


            <tfoot>
                
            </tfoot>
            
            <tbody>
            </tbody>
        </table>
	    	
    </div>


    @include ('staff.stafftemplates')

@endsection

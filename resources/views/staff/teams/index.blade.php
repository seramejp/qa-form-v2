@extends('layouts.app')

@section('main-content')
	<h3 class="text-white">{{$team->name}}</h3>
	<h5 class="text-white">Index - This form will show all the entries under this team, from all the raters.</h5>
@endsection

@section('below-main-content')
	<div class="row animate-box">
            
			<table class="table table-hover" cellspacing="0" width="100%" id="teamformsindex">
				<thead>
					<tr>
						<th>ID</th>
						<th width="4%"></th> <!-- View -->
						<th>Team</th>
						<th width="15%">Who?</th>
						<th width="15%">Auditor</th>
						<th>Trans Ref</th>
						<th width="15%">Trans Date</th>
						<th width="15%">Audit Date</th>
						<th>Total Score</th>
					</tr>
				</thead>
				


				<tfoot>
					<tr>
						<th colspan="9" rowspan="1">
							<a href="{{ url('/staff/'.$team->id.'/form')}}" class="btn btn btn-special" ><i class="fa fa-plus"></i> New Entry</a>
						</th>
					</tr>
				</tfoot>
				
				<tbody>
				</tbody>
			</table>
    </div>

@endsection

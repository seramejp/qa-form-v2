@extends('layouts.app')

@section('login-content')
    <div class="gtco-services gtco-section">
        <div class="gtco-container">
            <div class="row row-pb-md">

                <div class="col-md-6 col-sm-6 col-md-offset-3 service-wrap gtco-heading">
                    <div class="service animate-change">
                        <h2><i class="ti-user"></i> QM Form Login</h2>

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="name">Username</label>
                                <input id="email" type="text" class="form-control" name="username" value="{{ old('username') }}" required autofocus>

                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="name">Password</label>
                                <input type="password" class="form-control" id="password" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <input type="submit" class="btn btn btn-special" value="Log Me In">
                            </div>
                        </form>

                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection

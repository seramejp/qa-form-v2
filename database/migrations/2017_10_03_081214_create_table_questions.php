<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name'); //team name
            $table->text('guide')->nullable(); // a comprehensive guide for this question
            $table->integer('points')->default(0); //assigned points for this question

            $table->integer('sys_isactive')->default(1); //author of the team

            $table->integer('user_id')->unsigned()->default(1); //author of the team
            $table->integer('category_id')->unsigned()->default(0); //author of the team
            $table->integer('team_id')->unsigned()->default(0); //author of the team
            
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions'); 
    }
}

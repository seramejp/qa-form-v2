<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('team_id')->unsigned(); //
            
            $table->string('label')->default('Label');
            $table->string('type')->default('text'); // either text or select option
            $table->string('placeholder')->nullable(); // either text or select option
            $table->text('options')->nullable();
            $table->integer('order')->default(1); // the order to which this field will appear

            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields');    
    }
}

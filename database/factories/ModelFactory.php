<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'username' => $faker->unique()->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'sys_accesslevel' => 1
    ];
});


$factory->define(App\Team::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->firstNameMale,
        'descr' => $faker->sentence(4),
        'remarks' => $faker->paragraph(),
        'user_id' => factory('App\User')->create()->id,
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->firstNameFemale,
        'descr' => $faker->sentence(4),
        'remarks' => $faker->paragraph(),
    ];
});

$factory->define(App\Question::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->sentence(4),
        'guide' => $faker->paragraph(),
        'points' => 10,
        'sys_isactive' => 1,
    ];
});


$factory->define(App\Transaction::class, function (Faker\Generator $faker) {
    return [
        'person' => $faker->randomElement([$faker->name, $faker->name, $faker->name, $faker->name, $faker->name, $faker->name, $faker->name, $faker->name, $faker->name, $faker->name]),
        'transaction_reference' => $faker->paragraph(),
        'transaction_date' => $faker->date(),
        'remarks' => $faker->sentence(2),

        'team_id' => App\Team::all()->random()->id,
        'user_id' => App\User::all()->random()->id,
    ];
});


$factory->define(App\Answer::class, function (Faker\Generator $faker) {
    return [
        'theanswer' => $faker->randomElement(["yes", "no"]),
    ];
});
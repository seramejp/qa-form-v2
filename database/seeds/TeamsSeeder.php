<?php

use Illuminate\Database\Seeder;

class TeamsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Team::class, 5)
        	->create()
        	->each(function($team) {
	    		$team->categories()->saveMany(factory(App\Category::class, 5)
	    			->create()
	    			->each(function($category) use ($team) {
	    				$category->questions()->saveMany(factory(App\Question::class, 4))->create([
	    						"category_id" => $category->id,
	    						"team_id" => $team->id
	    					]);
    				})
				);
	  		});
    }
}

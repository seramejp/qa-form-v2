<?php

use Illuminate\Database\Seeder;

class TransactionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Transaction::class, 50)
        	->create()
        	->each(function($trans) {
	    			$team = App\Team::find($trans->team_id);

	    			foreach ($team->questions as $question) {
	    				# Create answer
	    				$trans->answers()->save(factory(App\Answer::class)->create([
	    					"user_id" => $trans->user_id,
	    					"transaction_id" => $trans->id,
	    					"question_id" => $question->id
	    					]));
	    			}
	  		});
    }
}

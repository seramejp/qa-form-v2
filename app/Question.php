<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $guarded = array();

    public function team (){
        return $this->belongsTo(Team::class)->select("id", "name");
    }

    public function category (){
        return $this->belongsTo(Category::class)->select("id", "name", "team_id");
    }

    public function answer (){
        return $this->belongsTo(Answer::class);
    }

    public function answers (){
        return $this->hasMany(Answer::class, "question_id", "id");
    }


}

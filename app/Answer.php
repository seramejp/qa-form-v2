<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Answer extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    
    public function question (){
        return $this->hasOne(Question::class, "id", "question_id");
    }

    public function transaction (){
        return $this->belongsTo(Transaction::class, "transaction_id", "id");
    }
}

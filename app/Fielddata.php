<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fielddata extends Model
{
    protected $table = "fieldsdata";
    

    public function transaction (){
        return $this->belongsTo(Transaction::class);
    }

    public function field (){
        return $this->belongsTo(Field::class, "field_id", "id");
    }

}

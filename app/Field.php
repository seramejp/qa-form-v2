<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    public function alldata (){
        return $this->hasMany(Fielddata::class, "field_id", "id");
    }  
}

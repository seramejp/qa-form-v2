<?php 

namespace App\Traits;

use App\Transaction;
/*

	When deleting a Category, delete also the questions under that category. 
	
	Categories
		|_ Questions
				|_ Answers

	Transactions
		|_ Answers
		|	|_ Questions
		|_ Team

*/

trait ComputeTotalsTrait
{
		
    	/**
     * Show details for Category
     * @param Category $category
     * @return void
     */
    public function deletingCategory (Category $category){

            $category->delete();

            foreach ($category->questions as $question) {
            		$question->delete();

            		foreach ($question->answers as $answer) {
            				$answer->delete();
            		}
            }
	        


    }



}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;
use App\Category;
use App\Question;
use App\Transaction;
use Auth;

use App\Traits\ComputeTotalsTrait;

class CategoryController extends Controller
{

    use ComputeTotalsTrait;

    /**
     * Controller for Categories
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Index for Categories
     *
     * @return View categories.index
     */
    public function index (){
    	$teams = Team::select("id", "name")->orderBy("name")->get();
        return view('admin.categories.index', compact('teams'));   
    }


    /**
     * Index for Categories
     *
     * @return Collection $categories
     */
    public function indexData (){
        $categories = Category::select('id', 'name', 'team_id', 'user_id', 'descr', 'remarks')
            ->with('team')
            ->orderBy("name")
            ->get();
            
        return response()->json([
            'data' => $categories,        
        ]);
    }


    /**
     * Create Category
     *
     * @return void
     */
    public function create (Request $request){
        $newCat = new Category;
        $newCat->name = $request->name;
        $newCat->descr = $request->descr;
        $newCat->remarks = $request->remarks;
        $newCat->user_id = Auth::user()->id;
        $newCat->team_id = $request->teamid;
        $newCat->save();

        return $newCat->id;   
    }


    /**
     * Update for Categories
     *
     * @return void
     */
    public function update (Category $category, Request $request){
        $category->name = $request->name;
        $category->descr = $request->descr;
        $category->remarks = $request->remarks;
        $category->save();
    } 


    /**
     * Show details for Category
     * @param Category $category
     * @return void
     */
    public function showCategoryDetails (Category $category){

        return view("admin.categories.showcategorydetails", compact('category'));
    } 



    /**
     * Show details for Category
     * @param Category $category
     * @return void
     */
    public function getCategoriesForTeam (Team $team){
        return response()->json([
            'categories' => $team->categories,        
        ]);
    } 


    /**
     * Show details for Category
     * @param Category $category
     * @return void
     */
    public function deleteview (){
        $teams = Team::select("id", "name")->orderBy("name")->get();
        return view('admin.categories.delete', compact('teams'));   
    } 



    /**
     * Show details for Category
     * @param Category $category
     * @return void
     */
    public function deletecategory (Category $category){
            $this->deletingCategory($category);
            

            dd($category);
            return back();

            /*$this->computeTransactionTotal($category->team_id);
            $this->deleteFKeys("category", $category);

            return redirect("/admin/categories/delete");*/
    } 





}

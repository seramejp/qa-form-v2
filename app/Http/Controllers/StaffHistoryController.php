<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;
use App\Transaction;

class StaffHistoryController extends Controller
{
    /**
     * Controller for Admin
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }



    /**
     * Form View for Staff - blank form
     *
     * @return Collection $fields
     */
    public function index (Team $team){

    	$teams = Team::all();

        return view('staff.history.index', compact('team', 'teams'));   
    }


    /**
     * Form View for Staff - blank form
     *
     * @return Collection $fields
     */
    public function search (Request $request){
        $opt = $request->searchOption;
        //Transaction Date will be replace with date encoded
        $resultSet = ($opt == "person" ? Transaction::where("person", $request->searchText)->get() : ($opt == "transactionreference" ? Transaction::where("transaction_reference", $request->searchText)->get() : ($opt == "dateencoded" ? Transaction::whereDate("created_at", ">=", $request->searchText)->get() : "" ) ) );

        return response()->json([
            'resultset' => $resultSet,        
        ]);
    }

}

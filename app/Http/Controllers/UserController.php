<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use Hash;

class UserController extends Controller
{
    
    /**
     * Controller for Teams
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Index for Teams - STAFF
     *
     * @return Collection $teams
     */
    public function index (){
    	$users = User::all();
        return view('admin.users.index', compact('users'));   
    }


    /**
     * Index for Teams - STAFF
     *
     * @return Collection $teams
     */
    public function edit (User $user){
        return view('admin.users.edit', compact('user'));   
    }


    /**
     * Save this user's update
     * 
     * @return view to Edit the User
     */
    public function update (Request $request, User $user){
        if ($user->sys_accesslevel == "1300135") {
            $user->update($request->except('created_at', 'password', 'remember_token', 'sys_accesslevel'));
        }else{
            $user->update($request->except('created_at', 'password', 'remember_token'));
        }
            

        if (!empty($request->password)) {
            $user->password = Hash::make($request->password);
            $user->save();
        }

        return redirect("/admin/users");
    }



    /**
     * View for Create a New User
     * 
     * @return view to Create new User
     */
    public function create (){
        return view('admin.users.create');
    }



    /**
     * View for Create a New User
     * 
     * @return view to Create new User
     */
    public function store (Request $request){

        $this->validate($request,[
                'name' => 'required|min:3',
                'username' => 'required|alpha_dash|unique:users|min:3|max:15',
                'password' => 'required|min:6',
            ],[
                'name.required' => ' The name field is required.',
                'name.min' => ' Name must be at least 3 characters.',
                'username.required' => ' Username field is required.',
                'username.alpha_dash' => ' Usernames must be alpha-numeric characters, as well as dashes and underscores, but no spaces.',
                'username.unique' => ' Usernames should be unique.',
                'username.min' => ' Username should be at least 3 characters.',
                'username.max' => ' Username should be at most 15 characters.',
                
                'password.required' => ' Password field is required.',
                'password.min' => ' Passwords should at least be 6 characters.',
            ]);

            

            $newuser = new User;

            $newuser->name = $request->name;
            $newuser->username = $request->username;
            $newuser->password = bcrypt($request->password);
            $newuser->sys_accesslevel = $request->sys_accesslevel;
            
            $newuser->save();    



        return redirect('/admin/users');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;
use App\Field;
use Auth;

class TeamController extends Controller
{
    /**
     * Controller for Teams
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Dashboard for Admins
     *
     * @return void
     */
    public function create (Request $request){
        $newTeam = new Team;
        $newTeam->name = $request->name;
        $newTeam->descr = $request->descr;
        $newTeam->remarks = $request->remarks;
        $newTeam->user_id = Auth::user()->id;
        $newTeam->save();

        //$this->createDefaultField($newTeam->id);

        return $newTeam->id;   
    }


    /**
     * Index for Teams
     *
     * @return Collection $teams
     */
    public function index (){
        return view('admin.teams.index');   
    }


    /**
     * Index for Teams
     *
     * @return Collection $teams
     */
    public function indexData (){
        $teams = Team::select("id", "name", "descr", "remarks")->orderBy("name")->get();
        return response()->json([
            'data' => $teams,        
        ]);
    }


    /**
     * Dashboard for Teams - Admin
     *
     * @return Collection $teams
     */
    public function dashboardData (){
        $teams = Team::select("id", "name")->with("categories", "questions")->orderBy("name")->get();

        return response()->json([
            'data' => $teams,        
        ]);
    }


    /**
     * Update for Teams
     *
     * @return void
     */
    public function update (Team $team, Request $request){
        $team->name = $request->name;
        $team->descr = $request->descr;
        $team->remarks = $request->remarks;
        $team->save();
    } 


    /**
     * Show details for Team
     * @param Team $team
     * @return void
     */
    public function showTeamDetails (Team $team){
        $fields = Field::where("team_id", $team->id)->orderBy("order")->get();
        return view("admin.teams.showteamdetails", compact('team', 'fields'));
    } 


    /**
     * Default Fields
     *
     * @return void
     */
    public function createDefaultField ($id){

        $defTransDate = new Field;
        $defTransDate->label = "Transaction Date";
        $defTransDate->type = "date";
        $defTransDate->order = 1;
        $defTransDate->team_id = $id;
        $defTransDate->save();

        $defRefNum = new Field;
        $defRefNum->label = "Reference Number";
        $defRefNum->type = "text";
        $defRefNum->order = 2;
        $defRefNum->team_id = $id;
        $defRefNum->save();

        $defPerson = new Field;
        $defPerson->label = "Person";
        $defPerson->type = "text";
        $defPerson->order = 3;
        $defPerson->team_id = $id;
        $defPerson->save();
    }

}

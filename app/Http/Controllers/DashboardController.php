<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Team;
use App\Transaction;
use \Carbon\Carbon;

class DashboardController extends Controller
{
    /**
     * Controller for Admin
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Dashboard for Admins
     *
     * @return void
     */
    public function adminDashboard (){
        $teams = Team::all();
        $top10trans = Transaction::whereYear("created_at", Carbon::now()->year)
            ->whereMonth("created_at", Carbon::now()->month)
            ->limit(10)
            ->orderBy("sys_total", "DESC")
            ->get();


        $mostRecent = Transaction::limit(10)
            ->orderBy("created_at", "DESC")
            ->get();

        //dd($teams, $top10trans, $mostRecent);

        return view("admin.dashboard", compact('teams', 'top10trans', 'mostRecent'));   
    }


    /**
     * Dashbaord for Staff
     *
     * @return void
     */
    public function staffDashboard (){
        $teams = Team::all();

        $top10trans = Transaction::whereYear("created_at", Carbon::now()->year)
            ->whereMonth("created_at", Carbon::now()->month)
            ->limit(10)
            ->orderBy("sys_total", "DESC")
            ->get();

        $mostRecent = Transaction::limit(10)
            ->orderBy("created_at", "DESC")
            ->get();

        return view("staff.dashboard", compact('teams', 'top10trans', 'mostRecent'));   
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Field;
use Auth;

class FieldController extends Controller
{
    /**
     * Controller for questions
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Create Field
     *
     * @return void
     */
    public function create (Request $request){
        $newField = new Field;
        $newField->label = $request->label;
        $newField->type = $request->type;
        $newField->placeholder = $request->placeholder;
        $newField->options = $request->options;
        $newField->order = $request->order;
        $newField->team_id = $request->teamid;

        $newField->save();

        return $newField->id;   
    }
}

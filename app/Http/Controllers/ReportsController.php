<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transaction;
use App\Team;
use App\Question;
use App\Answer;

use \Carbon\Carbon;
use Excel;

class ReportsController extends Controller
{
    /**
     * Controller for Teams
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }



    /**
     * Index for Teams
     *
     * @return Collection $teams
     */
    public function index (){
        return view('admin.reports.index');   
    }



    /**
     * Index for Teams
     *
     * @return Collection $teams
     */
    public function monthly (){
        $reports = Transaction::whereYear('created_at', '=', Carbon::now()->year)
              ->whereMonth('created_at', '=', Carbon::now()->month)
              ->orderBy('created_at')
              ->get();

        $currWeekTransactionAve = Transaction::select("person", "sys_total")
            ->whereBetween( 'created_at', [Carbon::today()->startOfWeek(), Carbon::today()->endOfWeek()] )
            ->get();


        return view('admin.reports.pages.monthlyscores', compact('reports', 'currWeekTransactionAve'));   
    }



    /**
     * Index for Teams
     *
     * @return Collection $teams
     */
    public function agentscoreperweek (){
        $reports = Transaction::orderBy('created_at')->get();
        $transact = collect();

        foreach ($reports as $trans) {
            $val = Transaction::whereBetween("created_at", [$trans->created_at->startOfWeek(), $trans->created_at->endOfWeek()])
                ->where("person", $trans->person)
                ->get()
                ->avg('sys_total');

            $transact->push(['id' => $trans->id, 'value' => $val]);
        }
        
        return view('admin.reports.pages.agentscoreweekly', compact('reports', 'transact'));   
    }


    /**
     * QM Weekly
     *
     * @return Collection $teams
     */
    public function qmweekly (){
        $qmweekly = Transaction::whereYear("created_at", Carbon::now()->year)
            ->get();

        return view('admin.reports.pages.qmweekly', compact('qmweekly'));   
    }


    /**
     * Those with errors in 
     *
     * @return Collection $teams
     */
    public function personsofinterest (){
        $poi = Transaction::whereHas("answers", function($q){
            $q->where("theanswer", "no");
        })->get();

        return view('admin.reports.pages.poi', compact('poi'));   
    }


    /**
     * Department WEekly Report
     *  Start at Week 1 for the year
     * @return Collection of average scores per department
     */
    public function deptweekly (){
        $teams = Team::all();

        $transactionsRaw = Transaction::select([
                \DB::raw("WEEK(created_at) as weeknum"),
                "team_id", 
                \DB::raw('AVG(sys_total) as sys_total'),

            ])
            ->whereYear("created_at", Carbon::now()->year)
            ->groupBy("weeknum", "team_id")
            ->orderBy('weeknum')->get();
        $transactions = $transactionsRaw->groupBy("weeknum");
        
        //dd($transactions);
        return view('admin.reports.pages.deptweekly', compact('teams', 'transactions'));   
    }

    
    /**
     * AJAX get details of the weekly report per team, per week
     *  Start at Week 1 for the year
     * @return Collection of average scores per department
     */
    public function deptweekly_getdetails ($team_id, $weeknum){
        $transactionDetails = Transaction::where("team_id", $team_id)
            ->whereRaw('WEEK(created_at) = ?', [$weeknum])
            ->with('user')
            ->get();

        return response()->json([
            'data' => $transactionDetails,        
        ]);
    }
    //


    /**
     * QM Weekly
     *
     * @return Collection $teams
     */
    public function qmweeklyexport (Transaction $trans){
        
        Excel::create('QMWeekly_'.$trans->person, function($excel) use ($trans) {
            $excel->sheet('Sheet1', function($sheet) use ($trans) {
                $sheet->loadView('admin.reports.exports.qmweekly')->with("trans", $trans);
            });
        })->export('xlsx');

    }


    /**
     * Agent WEekly Performance Report
     *  Start at Current week, backtrack up to 5 weeks back
     * @return Collection of average scores per department
     */
    public function agentweeklyperformance_basic (){
        $transactionsRaw = Transaction::selectRaw("WEEK(created_at) as weeknum, person, avg(sys_total) as sys_total, team_id")
            ->whereBetween(\DB::raw("WEEK(created_at)"), [\Carbon\Carbon::now()->weekOfYear-5, (\Carbon\Carbon::now()->weekOfYear)])
            ->whereYear("created_at", Carbon::now()->year)
            ->groupBy("person", "weeknum", "team_id")
            ->orderBy('person')->get();

        $weekOfTheYear = \Carbon\Carbon::now()->weekOfYear;
        $weeknumbers = array( $weekOfTheYear-4,
                        $weekOfTheYear -3,
                        $weekOfTheYear -2,
                        $weekOfTheYear -1,
                        $weekOfTheYear
                    );

        $transactions = $transactionsRaw->groupBy("person");

        return view('admin.reports.pages.agentweeklyperformance', compact( 'transactions', 'weeknumbers'));   
    }


    /**
     * Agent WEekly Performance Report
     *  Start at Current week, backtrack up to 5 weeks back
     * @return Collection of average scores per department
     */
    public function agentweeklyperformance_filtered ($weekfrom, $weekto){

        $transactionsRaw = Transaction::selectRaw("WEEK(created_at) as weeknum, person, avg(sys_total) as sys_total, team_id")
            ->whereBetween(\DB::raw("WEEK(created_at)"), [$weekfrom, $weekto])
            ->whereYear("created_at", Carbon::now()->year)
            ->groupBy("person", "weeknum", "team_id")
            ->orderBy('person')->get();


        $weeknumbers = array();
            
        for ($i=intval($weekfrom); $i <= intval($weekto); $i++) { 
            $weeknumbers[] = intval($i);
        }
        

        $transactions = $transactionsRaw->groupBy("person");

        /*foreach ($weeknumbers as  $wk) {
            echo "Week ".$wk."<br>";
        }*/
        //dd($weekfrom, $weekto, intval($weekfrom), intval($weekto), $weeknumbers, $transactions);

        
        return view('admin.reports.pages.agentweeklyperformance', compact('transactions', 'weeknumbers'));   
    }



    /**
     * Checkpoint Summary
     *  Start at Current week, backtrack up to 5 weeks back
     * @return Collection of average scores per department
     */
    public function checkpointsummary (){

        $weekOfTheYear = \Carbon\Carbon::now()->weekOfYear;
        $weeknumbers = array( $weekOfTheYear-4,
                        $weekOfTheYear -3,
                        $weekOfTheYear -2,
                        $weekOfTheYear -1,
                        $weekOfTheYear
                    );
        
        $answersNo = \DB::table('answers')
                        ->select("questions.name", "transactions.person", \DB::raw("week(transactions.created_at) as weeknum"), "teams.name as dept")
                        ->join("questions", "answers.question_id", "questions.id")
                        ->join("transactions", "answers.transaction_id", "transactions.id")
                        ->join("teams", "teams.id", "transactions.team_id")
                        ->where("answers.theanswer", "no")
                        ->whereBetween(\DB::raw("WEEK(transactions.created_at)"), [\Carbon\Carbon::now()->weekOfYear-5, (\Carbon\Carbon::now()->weekOfYear)])
                        ->get();

        $transactions = $answersNo->groupBy("name");
        //dd($transactions);

        return view('admin.reports.pages.checkpointsummary', compact('weeknumbers', 'transactions'));   
    }


    /**
     * Agent WEekly Performance Report
     *  Start at Current week, backtrack up to 5 weeks back
     * @return Collection of average scores per department
     */
    public function checkpointsummary_filtered ($weekfrom, $weekto){

        $weeknumbers = array();
            
        for ($i=intval($weekfrom); $i <= intval($weekto); $i++) { 
            $weeknumbers[] = intval($i);
        }
        
        $answersNo = \DB::table('answers')
                        ->select("questions.name", "transactions.person", \DB::raw("week(transactions.created_at) as weeknum"), "teams.name as dept")
                        ->join("questions", "answers.question_id", "questions.id")
                        ->join("transactions", "answers.transaction_id", "transactions.id")
                        ->join("teams", "teams.id", "transactions.team_id")
                        ->where("answers.theanswer", "no")
                        ->whereBetween(\DB::raw("WEEK(transactions.created_at)"), [$weekfrom, $weekto])
                        ->get();

        $transactions = $answersNo->groupBy("name");
        //dd($transactions);

        return view('admin.reports.pages.checkpointsummary', compact('weeknumbers', 'transactions'));  
    }



    /**
     * Department WEekly Report
     *  Start at Week 1 for the year
     * @return Collection of average scores per department
     */
    public function deptmonthly (){
        $teams = Team::all();

        $transactionsRaw = Transaction::select([
                \DB::raw("MONTH(created_at) as monthnum"),
                "team_id", 
                \DB::raw('AVG(sys_total) as sys_total'),

            ])
            ->whereYear("created_at", Carbon::now()->year)
            ->groupBy("monthnum", "team_id")
            ->orderBy('monthnum')->get();
        $transactions = $transactionsRaw->groupBy("monthnum");
        
        return view('admin.reports.pages.deptmonthly', compact('teams', 'transactions'));   
    }
}

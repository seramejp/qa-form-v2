<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $guarded = array();

    public function team (){
        return $this->belongsTo(Team::class)->select("id", "name");
    }

    public function questions (){
        return $this->hasMany(Question::class, "category_id", "id");
    }
}

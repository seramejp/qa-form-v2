<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    protected $guarded = array();

    public function categories (){
        return $this->hasMany(Category::class, "team_id", "id");
    }

    public function questions (){
        return $this->hasMany(Question::class, "team_id", "id");
    }

	public function transactions (){
        return $this->hasMany(Transaction::class, "team_id", "id");
    }    
    
}

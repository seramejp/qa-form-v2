/*

	Scripts for the Team

*/

function createTeam(){
	let name = $("#at-name").val();
	let descr = $("#at-descr").val();
	let remarks = $("#at-remarks").val();

	let tablehost = $("#at-tablehost").val();

	$.post(globalUrl + '/admin/teams/create', 
		{
			_token: globalToken,
			_method: 'POST',
			name: name,
			descr: descr,
			remarks: remarks

		}, function(data, textStatus, xhr) {
		/*optional stuff to do after success */

			swal('Yey!', 'You created a Team!', 'success' );
			if (tablehost == "db-teams") {
				var tbl = $("#db-teams").DataTable();
				tbl.rows.add([
						{
		                    id: data,
							editrow: '<a href="'+globalUrl+'/admin/teams/'+data+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the team and it\'s contents."><i class="ti-share"></a>',
							name: name,
							categories: "0",
							questions: "0"
						}
	                ] ).draw();
			}else{
				var tbl = $("#teamstable").DataTable();
				tbl.rows.add([
						{
							id: data,
							editrow: '<a href="'+globalUrl+'/admin/teams/'+data+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the team and it\'s contents."><i class="ti-share"></a>',
							name: name,
							descr: descr,
							remarks: remarks
						}
					]).draw();
			}

			document.getElementById('fAddTeam').reset();

				
	});

}


function updateTeam(){
	let name = $("#et-name").val();
	let descr = $("#et-descr").val();
	let remarks = $("#et-remarks").val();

	let id = $("#et-teamid").val();
	let tablehost = $("#et-tablehost").val();
	let rowindex = $("#et-rowindex").val();

	$.post(globalUrl + '/admin/teams/update/' + id, 
		{
			_token: globalToken,
			_method: 'PATCH',
			name: name,
			descr: descr,
			remarks: remarks

		}, function(data, textStatus, xhr) {
		/*optional stuff to do after success */

			if (tablehost == "db-teams") {
				var tbl = $("#db-teams").DataTable();
			}else{
				var tbl = $("#teamstable").DataTable();
				var d = tbl.row(rowindex).data();
                d.name = name;
                d.descr = descr;
                d.remarks = remarks;

                tbl.row(rowindex).data(d).draw();
			}
			swal('Yes!', 'You just updated this team!', 'success' );

			document.getElementById('fEditTeam').reset();
	});

}
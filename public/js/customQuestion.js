/*

	Scripts for the Team

*/

function createQuestion(){
	let teamid = $("#aq-team option:selected").val();
	let teamname = $("#aq-team option:selected").text();

	let categoryid = $("#aq-category option:selected").val();
	let categoryname = $("#aq-category option:selected").text();

	let name = $("#aq-name").val();
	let points = $("#aq-points").val();
	let guide = $("#aq-guide").val();

	$.post(globalUrl + '/admin/questions/create', 
		{
			_token: globalToken,
			_method: 'POST',
			teamid: teamid,
			categoryid: categoryid,
			name: name,
			points: points,
			guide: guide

		}, function(data, textStatus, xhr) {
		/*optional stuff to do after success */

			swal('Question Created!', 'Yeah, it\'s under Category '+ categoryname +', '+ teamname +' Team!', 'success' );
			
			var tbl = $("#questionstable").DataTable();
			tbl.rows.add([
					{
	                    id: data,
						editrow: '<a href="'+globalUrl+'/admin/questions/'+data+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the category details and it\'s questions."><i class="ti-share"></a>',
						team: {id: teamid, name: teamname},
						category: {id: categoryid, name: categoryname, team_id: teamid},
						name: name,
						points: points,
						guide: guide,
					}
                ] ).draw();

			//$("#tdtable_"+categoryid+" > tbody").append("<tr><td>"+name+"</td><td>"+points+"</td><td>"+guide+"</td></tr>");
			$("#tdtable_"+categoryid+" > tbody").append("<tr id='scdtrquestion_"+data+"' onclick='javascript:editQuestion("+data+");' style='cursor:pointer;' data-categoryid='"+categoryid+"' data-categoryname='"+categoryname+"' data-teamid='"+teamid+"' data-teamname='"+teamname+"'><td>"+name+"</td><td>"+points+"</td><td>"+guide+"</td></tr>");
			let totalpts = parseInt($("#detailswrapper #points_" +categoryid).text());
			$("#detailswrapper #points_" +categoryid).text(parseInt(totalpts) + parseInt(points));

			//$("#tdtable_questions > tbody").append("<tr><td>"+name+"</td><td>"+points+"</td><td>"+guide+"</td></tr>");
			document.getElementById('fAddQuestion').reset();
				
	});

}


function updateQuestion(){
	let teamid = $("#eq-team option:selected").val();
	let teamname = $("#eq-team option:selected").text();
	let categoryid = $("#eq-category option:selected").val();
	let categoryname = $("#eq-category option:selected").text();
	let name = $("#eq-name").val();
	let points = $("#eq-points").val();
	let guide = $("#eq-guide").val();

	let id = $("#eq-questionid").val();
	let rowindex = $("#eq-rowindex").val();

	$.post(globalUrl + '/admin/questions/update/' + id, 
		{
			_token: globalToken,
			_method: 'PATCH',
			categoryid: categoryid,
			name: name,
			points: points,
			guide: guide

		}, function(data, textStatus, xhr) {
		/*optional stuff to do after success */
			if (rowindex=="-199") {
				let oldcatid = $("#scdtrquestion_"+id).data("categoryid");
				let oldtotalpts = parseInt($("#points_"+oldcatid).text());
				let oldpts = parseInt($("#scdtrquestion_"+id).find("td:nth-child(2)").text());
				$("#points_"+oldcatid).text(oldtotalpts - oldpts);

				$("#scdtrquestion_" + id).remove();
				//recompute total here

				$("#tdtable_"+categoryid+" > tbody").append("<tr id='scdtrquestion_"+id+"' onclick='javascript:editQuestion("+id+");' style='cursor:pointer;' data-categoryid='"+categoryid+"' data-categoryname='"+categoryname+"' data-teamid='"+teamid+"' data-teamname='"+teamname+"'><td>"+name+"</td><td>"+points+"</td><td>"+guide+"</td></tr>");
				
				let tr = $("#scdtrquestion_" + id);

				tr.find("td:nth-child(1)").text(name);
				tr.find("td:nth-child(2)").text(points);
				tr.find("td:nth-child(3)").text(guide);
				let sum = 0;
				$('#tdtable_'+categoryid+' tbody').find("tr td:nth-child(2)").each(function(index, el) {
					sum += parseInt($(el).text());
				});
				$("#points_"+categoryid).text(sum);

			}else{
				var tbl = $("#questionstable").DataTable();
				var d = tbl.row(rowindex).data();
				d.category.id = categoryid;
				d.category.name = categoryname;
	            d.name = name;
	            d.points = points;
	            d.guide = guide;

	            tbl.row(rowindex).data(d).draw();
			}



			swal('Question Updated!', 'You just updated this question!', 'success' );

			document.getElementById('fEditQuestion').reset();
			
	});



}


function editQuestion(questionid){
    let tr = $("#scdtrquestion_"+questionid);

    $("#mEditQuestion").modal('toggle');

    $("#eq-name").val(tr.find("td:nth-child(1)").text());
    $("#eq-points").val(tr.find("td:nth-child(2)").text());
    $("#eq-guide").val(tr.find("td:nth-child(3)").text());

    $("#eq-team").empty().append(new Option(tr.data("teamname"), tr.data("teamid")));
    $("#eq-category").val(tr.data("categoryid")).change();

    $("#eq-questionid").val(questionid);
    $("#eq-rowindex").val("-199");

}
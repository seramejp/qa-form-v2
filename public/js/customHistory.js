

function loadHistoryTable(){
	let searchText = $("#searchText").val();
	let searchOption = $("#searchConceptList option:selected").val();

	$.post(globalUrl + '/staff/history/search', 
		{	
			_token: globalToken,
			_method: 'POST',
			searchText: searchText,
			searchOption: searchOption

		}, function(data, textStatus, xhr) {
			$("#formshistory > tbody").empty();

			if (data.resultset.length > 0) {
				
				$.each(data.resultset, function(index, el) {

					// JQUERY WAY

					var $template = $("#historytablerows");
			        var node = $template.prop('content');
			        var $content = $(node).find('tr').prop("id", "historytr_"+el.id);
			        var tds = $content.find('td');

            		tds.eq(0).empty().append($("<a/>").attr('href', globalUrl + "/staff/" + el.id + "/details").append($("<i/>").addClass('ti-share')));
					tds.eq(1).text(el.created_at);
					tds.eq(2).text(el.person);
					tds.eq(3).text(el.transaction_reference);
					tds.eq(4).text(el.transaction_date);
					tds.eq(5).text(el.sys_total);
					tds.eq(6).text(el.remarks);

					$("#formshistory tbody").append($($template.html()));
				});
			}
	});
	
}
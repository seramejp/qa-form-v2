var d = new Date();
var nowYear = d.getFullYear();
var nowMonth = d.getMonth();

$(document).ready(function(){

    //region Datatable Definitions
    /*
        Datatable Definitions
    */
    var dbtTeams = $("#db-teams").DataTable({
        ajax: globalUrl + '/admin/teams/getdashboarddata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/admin/teams/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the team and it\'s contents."><i class="ti-share"></a>';
                }
            }, 
            { "data": "name" }, //2
            { "data": "categories" }, //3
            { "data": "questions" }, //4
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [3,4],
                "render": function ( data, type, full, meta ) {
                    return data.length;
                }
            }
        ],
        deferRender: true,
        bPaginate: false,
        bLengthChange: false,
        bFilter: false,
        info: false,
        ordering: false
    });

    var ttTeams = $("#teamstable").DataTable({
        ajax: globalUrl + '/admin/teams/getindexdata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/admin/teams/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the team and it\'s contents."><i class="ti-share"></a>';
                }
            },
            { "data": "name" }, //2
            { "data": "descr" }, //3
            { "data": "remarks" }, //4
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        deferRender: true
    });

    var itCategories = $("#categoriestable").DataTable({
        ajax: globalUrl + '/admin/categories/getindexdata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/admin/categories/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the category details and it\'s questions."><i class="ti-share"></a>';
                }
            },
            { "data": "team" }, //2
            { "data": "name" }, //2
            { "data": "descr" }, //3
            { "data": "remarks" }, //4
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": 2,
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            }
        ],
        deferRender: true
    });

    var itQuestions = $("#questionstable").DataTable({
        ajax: globalUrl + '/admin/questions/getindexdata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    //return '<a href="'+globalUrl+'/admin/questions/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the question details."><i class="ti-share"></a>';
                    return '';
                }
            },
            { "data": "team" }, //2
            { "data": "category" }, //2
            { "data": "name" }, //2
            { "data": "points" }, //3
            { "data": "guide" }, //3
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": 2,
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            },
            {
                "targets": 3,
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            }
        ],
        deferRender: true
    });


    var dbsTeams = $("#dbstaff-teams").DataTable({
        ajax: globalUrl + '/staff/teams/getdashboarddata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/staff/'+data.id+'/index" data-toggle="tooltip" data-placement="top" title="Click to view the team and it\'s contents."><i class="ti-share"></a>';
                }
            }, 
            { "data": "name" }, //2
            { "data": "transactions" }, //3
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [3],
                "render": function ( data, type, full, meta ) {
                    return data.length;
                }
            }
        ],
        deferRender: true,
        bPaginate: false,
        bLengthChange: false,
        bFilter: false,
        info: false,
        ordering: false
    });


    //Index for Staff Team
    var tfiTeam = $("#teamformsindex").DataTable({
        ajax: globalUrl + '/staff/'+globalCurrentTeam+'/getindexdata',
        columns:    [
            { "data": "id"},//0
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/staff/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view this forms details."><i class="ti-share"></a>';
                }
            },//1
            { "data": "team" }, //2
            { "data": "person" }, //3
            { "data": "user" }, //4
            { "data": "transaction_reference" }, //6
            { "data": "transaction_date" }, //7
            { "data": "created_at" }, //8
            { "data": "sys_total" }, //9
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [2,4],
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            },
           
        ],
        deferRender: true,
        deferLoading: 25,
    });


    var deleteCategories = $("#categoriestabledelete").DataTable({
        ajax: globalUrl + '/admin/categories/getindexdata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    //return '<a href="'+globalUrl+'/admin/categories/delete/'+data.id+'" data-toggle="modal" data-target="#modalConfirmDeleteAsk"><i class="ti-trash"></a>';
                    return '<a href="" data-toggle="modal" data-target="#modalConfirmDeleteAsk" data-id="'+data.id+'"><i class="ti-trash"></a>';
                }
            },
            { "data": "team" }, //2
            { "data": "name" }, //2
            { "data": "descr" }, //3
            { "data": "remarks" }, //4
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": 2,
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            }
        ],
        deferRender: true
    });


    var itQuestionsDelete = $("#questionstabledelete").DataTable({
        ajax: globalUrl + '/admin/questions/getindexdata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="" data-toggle="modal" data-target="#modalConfirmDeleteQuestion" data-id="'+data.id+'"><i class="ti-trash"></a>';
                }
            },
            { "data": "team" }, //2
            { "data": "category" }, //2
            { "data": "name" }, //2
            { "data": "points" }, //3
            { "data": "guide" }, //3
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": 2,
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            },
            {
                "targets": 3,
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            }
        ],
        deferRender: true
    });

    //endregion


    //region Datatable Reports
    var reportsTableMonthly = $("#reporttablemonthly").DataTable({
        "dom": 'Bfrtip',
        "buttons": [
            {
                extend: 'csv',
                text: '<i class="ti-download"></i> CSV',
                className: 'btn btn-special',
                title: 'Monthly_Report_' + nowMonth + "_" + nowYear
            }
        ],
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": false,
        "ordering": false
    });

    var reportsTableAgentPerWeek = $("#reporttableagentweekly").DataTable({
        "dom": 'Bfrtip',
        "buttons": [
            {
                extend: 'csv',
                text: '<i class="ti-download"></i> CSV',
                className: 'btn btn-special',
                title: 'Agent_Score_Per_Week'
            }
        ],
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": false,
        "ordering": false
    });

    var reportsTableQMWeekly = $("#reporttableqmweekly").DataTable({
        "dom": 'Bfrtip',
        "buttons": [
            {
                extend: 'csv',
                text: '<i class="ti-download"></i> CSV',
                className: 'btn btn-special',
                title: 'QM_Sheet_Weekly'
            }
        ],
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "ordering": false
    });

    var reportsTablePOI = $("#reporttablepoi").DataTable({
        "dom": 'Bfrtip',
        "buttons": [
            {
                extend: 'csv',
                text: '<i class="ti-download"></i> CSV',
                className: 'btn btn-special',
                title: 'Transactions_with_Errors_' + nowYear
            }
        ],
        "bPaginate": true,
        "bLengthChange": true,
        "bFilter": true,
        "ordering": false
    });

    var reportstableDeptWeekly = $("#reporttableDeptWeekly").DataTable({
        footerCallback: function ( row, data, start, end, display ) {
                
                var api = this.api(), data;
                let columnLength = row.cells.length;
                
                                
                for(var indx = 1; indx<=columnLength-1; indx++){
                    htmElem = $(api.column(indx, {page:"current"}).data());
                    let sumx = 0;
                    if (indx == 7) {
                            console.log("BReAK HERE");
                    }
                    for(var x = 0; x<$(htmElem).length; x++){

                        sumx += parseInt( $(htmElem[x]).text() == "" ? 0 : $(htmElem[x]).text() );
                    }

                    $( api.column( indx ).footer() ).html(
                            sumx/$(htmElem).length
                    );
                }
        },

        "dom": 'Bflrtip',
        "buttons": [
            {
                extend: 'csv',
                text: '<i class="ti-download"></i> CSV',
                className: 'btn btn-special',
                title: 'Department_weekly_performance_' + nowYear
            }
        ],
        "bPaginate": true,
        "pageLength": 100,
        "lengthChange": true,
        "bFilter": false,
        "ordering": false
    });
    
    var reportstableAgentWeeklyDetails = $("#reportstableAgentWeeklyDetails").DataTable({
        initComplete: function () {
            this.api().columns([0,1]).every( function () {
                var column = this;
                
                var select = $('<select class="form-control input-sm"><option value="">All</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
                  
            } );

        },
        
        footerCallback: function ( row, data, start, end, display ) {
                var api = this.api(), data;
                let columnLength = row.cells.length;
                                
                for(var indx = 2; indx<=columnLength-1; indx++){
                    $( api.column( indx ).footer() ).html(
                            api.column(indx, {page:"current"}).data().average().toFixed(2)
                    );
                }
        },
        "dom": 'Bflrtip',
        "buttons": [
            {
                extend: 'csv',
                text: '<i class="ti-download"></i> CSV',
                className: 'btn btn-special',
                title: 'Agent_weekly_performance_details_' + nowYear,
                footer: true,
            }
        ],
        "bPaginate": true,
        "pageLength": 25,
        "lengthChange": true,
        "bFilter": true,
        "ordering": false
    });
    $("#reportstableAgentWeeklyDetails_filter").addClass('hidden');
    //reportstableAgentWeeklyDetails.row.add(getColumnDataForReports(reportstableAgentWeeklyDetails)).draw();

    var reportstableCheckpointSummary = $("#reportstableCheckpointSummary").DataTable({
        initComplete: function () {
            this.api().columns([0,1,2]).every( function () {
                var column = this;
                var select = $('<select class="form-control input-sm"><option value="">All</option></select>')
                    .appendTo( $(column.header()).empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );
 
                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );
 
                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        },
        "dom": 'Bflrtip',
        "buttons": [
            {
                extend: 'csv',
                text: '<i class="ti-download"></i> CSV',
                className: 'btn btn-special',
                title: 'Checkpoint_Summary'
            }
        ],
        "bPaginate": true,
        "lengthChange": true,
        "pageLength": 25,
        "bFilter": true,
        "ordering": false
    });

    //endregion



    //region Datatable Events
    /*
        Datatable Events
    */
    $("#teamstable").on('click', '#at-mAddTeam', function(event) {
        $("#at-tablehost").val('teamstable');
    });

    $("#db-teams").on('click', '#at-sAddTeam', function(event) {
        $("#at-tablehost").val('db-teams');
    });

    $("#categoriestable").on('click', '#ac-mAddCategory', function(event) {
        $("#ac-tablehost").val('categoriestable');
    });

    //endregion


    //region HTML Events
    $("body").on('click', '#std-mAddCategory', function(event) {
        teamid = $(this).data("teamid");
        teamname = $(this).data("teamname");

        $("#ac-tablehost").val('accordionTeamDetails');
        $('#ac-team').empty().append(new Option(teamname, teamid));
        $('#ac-team').selectpicker('refresh');
        $('#ac-team').val($('#ac-team option:first').val());

    });

    $('#fAddQuestion').on('changed.bs.select', '#aq-team', function (e) {
        $("#aq-category option").removeProp('disabled');
        $("#aq-category option").removeAttr('disabled');

        $("#aq-category option").each(function(index, el) {
            teamid = $(el).data("teamid");

            if (teamid != $("#aq-team option:selected").val()) {
                
                $(el).prop('disabled', "true");

                $('#aq-category').val($('#aq-category option:first').val());
                $('#aq-category').selectpicker('refresh');
                $('#aq-category').selectpicker('render');

            }
        });
    });

    $("#fAddQuestion").on('changed.bs.select', '#aq-category', function(event) {
        teamid = $(this).find("option:selected").data("teamid");

        $("#aq-team").val(teamid);
        $('#aq-team').selectpicker('refresh');
    });

    $("#detailswrapper").on('click', '#std-mAddQuestion', function(event) {
        let element = $(this);

        $('#aq-team').empty().append(new Option(element.data("teamname"), element.data("teamid")))
            .selectpicker('refresh')
            .val($('#aq-team option:first').val());
        $('#aq-category').empty().append(new Option(element.data("categoryname"), element.data("categoryid")))
            .selectpicker('refresh')
            .prop({"data-teamid": element.data("teamid")})
            .val($('#aq-category option:first').val());
    });

    $("body").on('click', '#scd-mAddQuestion', function(event) {
        let element = $(this);
        
        $('#aq-team').empty().append(new Option(element.data("teamname"), element.data("teamid")))
            .selectpicker('refresh')
            .val($('#aq-team option:first').val());
        $('#aq-category').empty().append(new Option(element.data("categoryname"), element.data("categoryid")))
            .selectpicker('refresh')
            .prop({"data-teamid": element.data("teamid")})
            .val($('#aq-category option:first').val());
    });

    $("#mAddField").on('change', 'input[type="radio"][name="optradio"]', function(event) {
         event.preventDefault();
         
         if (this.value == "text") {
            $("#twrapper").removeClass('hidden');
            $("#selectwrapper").addClass('hidden');

         }else if(this.value == "select"){
            $("#twrapper").removeAttr("class").addClass('hidden');
            $("#selectwrapper").removeClass('hidden');
         }else{
            $("#twrapper").addClass('hidden');
            $("#selectwrapper").addClass('hidden');
         }
     });

    $("#mAddField").on('change', 'input[type="radio"][name="optradio"]', function(event) {
         event.preventDefault();
         
         if (this.value == "text") {
            $("#twrapper").removeClass('hidden');
            $("#selectwrapper").addClass('hidden');

         }else if(this.value == "select"){
            $("#twrapper").removeAttr("class").addClass('hidden');
            $("#selectwrapper").removeClass('hidden');
         }else{
            $("#twrapper").addClass('hidden');
            $("#selectwrapper").addClass('hidden');
         }
     });
    
    $("#accordionGeneralInfo").on('click', '#af-mAddField', function(event) {
        $("#af-tablehost").val('tdtable_general');
        $("#af-teamid").val($(this).data('teamid'));
    });

    $("#reporttableDeptWeekly").on('click', '.weeklyreportdetails', function(event) {
            let teamname = $(this).data("teamname");
            let teamid = $(this).data("teamid");
            let weeknumber = $(this).data("weeknumber");
            let averagescore = $(this).data("averagescore");

            $("#mViewDetails #title_teamname").text(teamname);
            $("#mViewDetails #title_description").text("Average Score: " + averagescore + " Week #: " + weeknumber);

            $.get(globalUrl + '/admin/reports/weeklyreportdetails/' + teamid + '/' + weeknumber, 
                function(data, textStatus, xhr) {
                    if (data.data.length > 0) {
                        var html = "";
                        
                        $.each(data.data, function(index, el) {
                            let d = new Date(Date.parse(el.created_at));
                            let formatted = `${d.getFullYear()}-${d.getMonth() + 1}-${d.getDate()}`;
                            html += "<tr><td><a href='"+globalUrl+"/staff/"+el.id+"/details' target='_blank'><i class='ti-share'></i></a></td>"+
                                "<td>"+ el.person +"</td>" + 
                                "<td>"+ el.user.name +"</td>" + 
                                "<td>"+ el.transaction_reference +"</td>" + 
                                "<td>"+ el.transaction_date +"</td>" + 
                                "<td>"+ formatted +"</td>" + 
                                "<td>"+ el.sys_total +"</td></tr>";
                        });
                        $("#mViewDetails_table").empty().append(html);

                        var reportstableDeptWeeklyDetails = $("#tableViewWeeklyPerformanceDetails").DataTable({
                            "dom": 'Bflrtip',
                            "buttons": [
                                {
                                    extend: 'csv',
                                    text: '<i class="ti-download"></i> CSV',
                                    className: 'btn btn-special',
                                    title: 'Department_weekly_performance_details_for_team_' + teamname + '_ave_' + averagescore
                                }
                            ],
                            "bPaginate": true,
                            "lengthChange": true,
                            "bFilter": true,
                            "ordering": false
                        });

                        $(".dataTables_filter").addClass('pull-right');
                        $(".dataTables_length select").removeClass('input-sm');
                    }
            });
    });


    $("#modalConfirmDeleteAsk").on('show.bs.modal', function(event) {
        let id = $(event.relatedTarget).data("id");
        $("#btnYesDeleteCategory").attr('href', globalUrl+'/admin/categories/delete/'+ id);
    });

    $("#modalConfirmDeleteQuestion").on('show.bs.modal', function(event) {
        let id = $(event.relatedTarget).data("id");
        $("#btnYesDeleteQuestion").attr('href', globalUrl+'/admin/questions/delete/'+ id);
    });

    

    //endregion



    //region Datatable Events

    ttTeams.on('click', 'tbody tr td', function() {
            
            if (ttTeams.cell(this).index().column > 1) {
                let t = ttTeams.row(this).data();

                document.getElementById('fEditTeam').reset();
                $("#mEditTeam").modal('toggle');

                $("#et-teamid").val(t.id);
                $("#et-rowindex").val(ttTeams.row(this).index());
                $("#et-tablehost").val("teamstable");
                $("#et-name").val(t.name);
                $("#et-descr").val(t.descr);
                $("#et-remarks").val(t.remarks);
            }
            
        });

    itCategories.on('click', 'tbody tr td', function() {
            if (itCategories.cell(this).index().column > 1) {
                let t = itCategories.row(this).data();

                document.getElementById('fEditCategory').reset();
                $("#mEditCategory").modal('toggle');

                $("#ec-rowindex").val(itCategories.row(this).index());
                $("#ec-categoryid").val(t.id);
                
                $("#ec-team").empty().append(new Option(t.team.name, t.team.id));
                $("#ec-name").val(t.name);
                $("#ec-descr").val(t.descr);
                $("#ec-remarks").val(t.remarks);
            }
            
        });

    itQuestions.on('click', 'tbody tr td', function() {
            if (itQuestions.cell(this).index().column > 1) {
                var t = itQuestions.row(this).data();

                document.getElementById('fEditQuestion').reset();
                $("#mEditQuestion").modal('toggle');

                $("#eq-rowindex").val(itQuestions.row(this).index());
                $("#eq-questionid").val(t.id);
                
                $("#eq-team").empty().append(new Option(t.team.name, t.team.id));
                $("#eq-category").empty();

                //$("#eq-category").empty().append(new Option(t.category.name, t.category.id));
                $.get(globalUrl + '/admin/categories/team/'+t.team.id, function(data) {
                    console.log(data.categories);
                    var html = "";
                    $.each( data.categories, function( key, value ) {
                        html += '<option value="'+ value.id +'" '+ (value.id == t.category.id ? "selected" : "") +'>'+value.name+'</option>';
                    });
                    $("#eq-category").append(html);
                });
                
                
                $("#eq-name").val(t.name);
                $("#eq-points").val(t.points);
                $("#eq-guide").val(t.guide);
            }
            
        });
    //endregion


    $('body').tooltip({
        selector: '[data-toggle=tooltip]'
    });
    $(".segment-select").Segment();


    $("#aq-team").val($("#aq-category option:first").data("teamid"));
    $('#aq-team').selectpicker('refresh'); 

    $(".dataTables_filter").addClass('pull-right');
    $(".dataTables_length select").removeClass('input-sm');


    $("#searchConceptList").change(function(event) {
        switch(this.selectedIndex){
            case 0: //person
            case 1: // transaction_reference
                $("#searchText").removeAttr('type').attr({
                    type: 'text'
                });
                break;
            case 2: //transaction_date
                $("#searchText").removeAttr('type').attr({
                    type: 'date'
                });
                break;
        }
    });

});

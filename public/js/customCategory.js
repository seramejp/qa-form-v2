/*

	Scripts for the Team

*/

function createCategory(){
	let teamid = $("#ac-team option:selected").val();
	let teamname = $("#ac-team option:selected").text();
	let name = $("#ac-name").val();
	let descr = $("#ac-descr").val();
	let remarks = $("#ac-remarks").val();

	let hostwrapper = $("#ac-tablehost").val();

	$.post(globalUrl + '/admin/categories/create', 
		{
			_token: globalToken,
			_method: 'POST',
			teamid: teamid,
			name: name,
			descr: descr,
			remarks: remarks

		}, function(data, textStatus, xhr) {
		/*optional stuff to do after success */

			swal('Category Created!', 'Yeah, it\'s under'+ teamname +' Team!', 'success' );
			
			switch(hostwrapper){
				case 'categoriestable':
					var tbl = $("#categoriestable").DataTable();
					tbl.rows.add([
							{
			                    id: data,
								editrow: '<a href="'+globalUrl+'/admin/categories/'+data+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the category details and it\'s questions."><i class="ti-share"></a>',
								team: {id: teamid, name: teamname},
								name: name,
								descr: descr,
								remarks: remarks
							}
		                ] ).draw();
					break;

				case 'accordionTeamDetails':
					if ($("#accordionTeamDetails").length > 0) {
						var temp = document.querySelector('#teamDetailsTemplate');
						temp.content.querySelector(".panel-title a").href = "#tdcollapse_" + data;
						temp.content.querySelector(".panel-title a").textContent  = name;
						temp.content.querySelector(".panel-title span.pts").id  = "points_" + data;
						temp.content.querySelector(".panel-collapse.collapse").id = "tdcollapse_" + data;
						temp.content.querySelector(".panel-body table").id = "tdtable_" + data;
						temp.content.querySelector(".panel-body table thead th a").setAttribute("data-categoryid", data);
						temp.content.querySelector(".panel-body table thead th a").setAttribute("data-categoryname", name);
						temp.content.querySelector(".panel-body table thead th a").setAttribute("data-teamid", teamid);
						temp.content.querySelector(".panel-body table thead th a").setAttribute("data-teamname", teamname);
						
						document.querySelector("#accordionTeamDetails").appendChild(document.importNode(temp.content, true));
					}else{
						var temp = document.querySelector('#teamAccordion');
						temp.content.querySelector(".panel-title a").href = "#tdcollapse_" + data;
						temp.content.querySelector(".panel-title a").textContent  = name;
						temp.content.querySelector(".panel-title span.pts").id  = "points_" + data;
						temp.content.querySelector(".panel-collapse.collapse").id = "tdcollapse_" + data;
						temp.content.querySelector(".panel-body table").id = "tdtable_" + data;
						temp.content.querySelector(".panel-body table thead th a").setAttribute("data-categoryid", data);
						temp.content.querySelector(".panel-body table thead th a").setAttribute("data-categoryname", name);
						temp.content.querySelector(".panel-body table thead th a").setAttribute("data-teamid", teamid);
						temp.content.querySelector(".panel-body table thead th a").setAttribute("data-teamname", teamname);
						
						document.querySelector("#detailswrapper").appendChild(document.importNode(temp.content, true));

						$("#nodatainfo").remove();
					}


					break;


			}
				
			
			document.getElementById('fAddCategory').reset();
				
	});

}


function updateCategory(){
	let name = $("#ec-name").val();
	let descr = $("#ec-descr").val();
	let remarks = $("#ec-remarks").val();

	let id = $("#ec-categoryid").val();
	let rowindex = $("#ec-rowindex").val();

	$.post(globalUrl + '/admin/categories/update/' + id, 
		{
			_token: globalToken,
			_method: 'PATCH',
			name: name,
			descr: descr,
			remarks: remarks

		}, function(data, textStatus, xhr) {
		/*optional stuff to do after success */
			
			var tbl = $("#categoriestable").DataTable();
			var d = tbl.row(rowindex).data();
            d.name = name;
            d.descr = descr;
            d.remarks = remarks;

            tbl.row(rowindex).data(d).draw();
			
			swal('Category Updated!', 'You just updated this category!', 'success' );

			document.getElementById('fEditCategory').reset();
	});

}
/*

	Scripts for the Team

*/

function createField(){

	let label = $("#af-label").val();
	let type = $("input[name='optradio']:checked").val();
	let placeholder = $("#af-placeholder").val();
	let options = $("#af-options").val();
	let order = $("#af-order").val();

	let tablehost = $("#af-tablehost").val();
	let teamid = $("#af-teamid").val();

	$.post(globalUrl + '/admin/fields/create', 
		{
			_token: globalToken,
			_method: 'POST',
			label: label,
			type: type,
			placeholder: placeholder,
			options: options,
			order: order,
			teamid: teamid

		}, function(data, textStatus, xhr) {
		/*optional stuff to do after success */

			swal('Field Created!', '', 'success' );

			if (tablehost=="tdtable_general") {
				var temp = document.querySelector('#fieldrows');
				temp.content.querySelector("tr").id = "fieldrow_" + data;
				temp.content.querySelector("tr").setAttribute("onclick", "editField("+ data +");");
				var tds = temp.content.querySelectorAll("td");
				tds[0].textContent = label;
				tds[1].textContent = type;
				tds[2].textContent = placeholder;
				tds[3].textContent = options;
				tds[4].textContent = order;

				document.querySelector("#tdtable_general tbody").appendChild(document.importNode(temp.content, true));
			}

			document.getElementById('fAddField').reset();
				
	});

}


function updateField(fieldid){

	let tr = $("#tdtable_general"+ fieldid).val();

	$.post(globalUrl + '/admin/questions/update/' + id, 
		{
			_token: globalToken,
			_method: 'PATCH',
			name: name,
			points: points,
			guide: guide

		}, function(data, textStatus, xhr) {
		/*optional stuff to do after success */
			
			var tbl = $("#questionstable").DataTable();
			var d = tbl.row(rowindex).data();
            d.name = name;
            d.points = points;
            d.guide = guide;

            tbl.row(rowindex).data(d).draw();
			
			swal('Question Updated!', 'You just updated this question!', 'success' );

			document.getElementById('fEditQuestion').reset();
	});

}
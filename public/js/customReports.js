/*

	Scripts for Reports

*/




function form_agentweeklyperformance(){
	let from = $("#form_agentweeklyperformance input[name='weekFrom']").val().split("W");;
	let to = $("#form_agentweeklyperformance input[name='weekTo']").val().split("W");
	
	$("#form_agentweeklyperformance").prop({
		action: globalUrl + '/admin/reports/agentweeklyperformance/'+ from[1] + "/" + to[1]
	}).submit();
}


function form_checkpointsummary(){
	let from = $("#form_checkpointsummary input[name='weekFrom']").val().split("W");;
	let to = $("#form_checkpointsummary input[name='weekTo']").val().split("W");
	
	$("#form_checkpointsummary").prop({
		action: globalUrl + '/admin/reports/checkpointsummary/'+ from[1] + "/" + to[1]
	}).submit();
}



function getColumnDataForReports(table){
		let columnLength = table.columns().nodes().length;
		
		let columnData = ["", "Average"];

		for (var i = 2; i <= columnLength-1; i++) {
				columnData.push(table.column(i).data().average().toFixed(2));
		}
		columnData.push("");

		return columnData;
}
